﻿using Microsoft.EntityFrameworkCore;
using Telegram.Client.Database.Models;
using Telegram.Client.Db.Context;
using Telegram.Client.Db.Context.Entity;
using Telegram.Core.Services.Models.Enums;
using Telegram.Core.Services.Models.Messages;

namespace Telegram.Client.Database
{
    public class MessageRepository
    {
        public Guid SavePlainMessage(PlainMessageDto model)
        {
            using var db = new DatabaseContext();
            var message = new Message
            {
                SenderId = model.Sender,
                ChatId = model.Chat.Id,
                Content = model.Content,
                DateOfReceiving = DateTime.UtcNow,
                Reply = model.Reply
            };
            if (model.Id != Guid.Empty)
            {
                message.Id = model.Id;
            }
            db.Messages.Add(message);
            db.SaveChangesAsync();
            SaveMesageStatus(model, message);
            return message.Id;
        }

        public Guid SaveMediaMessage(MediaMessageDto model)
        {
            using var db = new DatabaseContext();
            var message = new Message
            {
                SenderId = model.Sender,
                ChatId = model.Chat.Id,
                Content = model.Content,
                FileId = model.FileId,
                MediaType = model.MediaType,
                DateOfReceiving = DateTime.UtcNow,
                Reply= model.Reply
            };
            if (model.Id != Guid.Empty)
            {
                message.Id = model.Id;
            }
            db.Messages.Add(message);
            db.SaveChangesAsync();
            SaveMesageStatus(model, message);
            return message.Id;
        }

        public void SaveSynchronizedPtpMesage(PlainMessageDto model, DateTime dateTime)
        {
            using var db = new DatabaseContext();
            var message = new Message
            {
                Id = model.Id,
                SenderId = model.Sender,
                ChatId = model.Chat.Id,
                Content = model.Content,
                DateOfReceiving = dateTime,
                Reply= model.Reply
            };
            db.Messages.Add(message);
            db.SaveChangesAsync();
            SaveMesageStatus(model, message);
        }

        public void SaveSynchronizedMediaMesage(MediaMessageDto model, DateTime dateTime)
        {
            using var db = new DatabaseContext();
            var message = new Message
            {
                Id = model.Id,
                SenderId = model.Sender,
                ChatId = model.Chat.Id,
                Content = model.Content,
                DateOfReceiving = dateTime,
                Reply = model.Reply,
                FileId= model.FileId,
                MediaType = model.MediaType,
            };
            db.Messages.Add(message);
            db.SaveChangesAsync();
            SaveMesageStatus(model, message);
        }

        public async Task MessageReceived(Guid messageId)
        {
            using var db = new DatabaseContext();
            var wasReceived = await db.MessageStatus
                .FirstOrDefaultAsync(x => x.MessageId == messageId);

            wasReceived.Status = MessageStatuses.Sent;

            var message = db.Messages.Find(wasReceived.MessageId);
            message.DateOfReceiving = DateTime.UtcNow;

            await db.SaveChangesAsync();
        }
        public void MessageRead(Guid chatId)
        {
            using var db = new DatabaseContext();
            var wasReceiveds = db.MessageStatus
                .Include(x => x.Message)
                .Where(x => x.Message.ChatId == chatId && x.Status == MessageStatuses.Sent)
                .ToList();
            foreach (var wasReceived in wasReceiveds)
            {
                wasReceived.Status = MessageStatuses.Read;
            }
            db.SaveChanges();
        }

        public List<MessageStatus> GetMessagesByChatId(Guid chatId)
        {
            using var db = new DatabaseContext();
            return db.MessageStatus
                .Include(x => x.Message)
                .Include(x => x.Message.Sender)
                .Where(x => x.Message.ChatId == chatId)
                .OrderBy(x => x.Message.DateOfReceiving)
                .ToList();
        }

        public Guid GetSecondUserIdByPtpChatId(Guid chatId, Guid firstUserId)
        {
            using var db = new DatabaseContext();
            var chat = db.PtpChats
                .Include(x => x.SecondUser)
                .FirstOrDefault(x => x.Id == chatId);
            if (chat.FirstUserId == firstUserId)
            {
                return chat.SecondUser.Id;
            }
            else
            {
                return chat.FirstUserId;
            }
        }

        private void SaveMesageStatus(BaseProtocolDto model, Message message)
        {
            using var db = new DatabaseContext();
            switch (model.Chat.ChatTypes)
            {
                case ChatTypes.PtpChat:
                    var recipientId = GetSecondUserIdByPtpChatId(model.Chat.Id, model.Sender);
                    var statusPtp = new MessageStatus
                    {
                        SenderId = model.Sender,
                        RecipientId = recipientId,
                        MessageId = message.Id
                    };
                    if (db.Users.First().Id != model.Sender)
                    {
                        statusPtp.Status = MessageStatuses.Sent;
                    }
                    db.MessageStatus.Add(statusPtp);
                    break;
                case ChatTypes.GroupChat:
                    if (model.Sender != db.Users.First().Id)
                    {
                        var statusGroup = new MessageStatus
                        {
                            SenderId = model.Sender,
                            RecipientId = db.Users.First().Id,
                            MessageId = message.Id
                        };
                        db.MessageStatus.Add(statusGroup);
                    }
                    else
                    {
                        var statusGroup = new MessageStatus
                        {
                            SenderId = model.Sender,
                            MessageId = message.Id
                        };
                        db.MessageStatus.Add(statusGroup);
                    }
                    break;
            }
            db.SaveChanges();
        }

        public FileIdInMessage GetFileIdInMessage(Guid messageId)
        {
            using var db = new DatabaseContext();
            var message = db.Messages.Find(messageId);
            if (message.FileId == Guid.Empty)
            {
                return null;
            }
            return new FileIdInMessage
            {
                FileId = message.FileId,
                MediaType = message.MediaType,
            };
        }

        public void DeleteMessage(Guid messageId)
        {
            using var db = new DatabaseContext();
            var message = db.Messages.Find(messageId);
            db.Messages.Remove(message);
            db.SaveChanges();
        }

        public ReplyMessage ReplyMessage(Guid messageId)
        {
            using var db = new DatabaseContext();
            var message = db.Messages.Find(messageId);
            if (message.Reply == Guid.Empty)
            {
                return null;
            }
            var replyMessage = db.Messages
                .Include(x => x.Sender)
                .FirstOrDefault(x => x.Id == message.Reply);
            var result = new ReplyMessage
            {
                Id = replyMessage.Id,
                Content = replyMessage.Content,
                SenderName = replyMessage.Sender.Username
            };
            switch (replyMessage.MediaType)
            {
                case MediaTypes.PhotoMessage:
                    result.PhotoMessage = true; 
                    break;
                case MediaTypes.FileMessage:
                    result.FileMessage = true; 
                    break;
            }
            return result;        
        }
    }
}
