﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Client.Database.Models
{
    public class FileIdInMessage
    {
        public Guid FileId { get; set; }
        public MediaTypes MediaType { get; set; }
    }
}
