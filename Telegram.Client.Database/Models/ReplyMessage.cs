﻿namespace Telegram.Client.Database.Models
{
    public class ReplyMessage
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
        public string SenderName { get; set; }
        public bool PhotoMessage { get; set; } = false;
        public bool FileMessage { get; set; } = false;
    }
}
