﻿namespace Telegram.Client.Database.Models
{
    public class MessageModel
    {
        public Guid MessageId { get; set; }
        public string Content { get; set; }
        public Guid SenderId { get; set; }
        public string SenderName { get; set; }
        public DateTime DateTime { get; set; }
    }
}
