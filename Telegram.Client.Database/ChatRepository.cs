﻿using Microsoft.EntityFrameworkCore;
using Telegram.Client.Db.Context;
using Telegram.Client.Db.Context.Entity;
using Telegram.Core.Services.Models.Enums;

namespace Telegram.Client.Database
{
    public class ChatRepository
    {
        public async Task SaveChat(BaseChat chat)
        {
            using var db = new DatabaseContext();
            switch (chat.ChatTypes)
            {
                case ChatTypes.PtpChat:
                    var ptpChat = (PtpChat)chat;
                    var userInDb = db.Users.Find(ptpChat.SecondUser.Id);
                    if (userInDb == null)
                    {
                        ptpChat.SecondUserId = ptpChat.SecondUser.Id;
                        db.PtpChats.Add(ptpChat);
                    }
                    else
                    {
                        db.PtpChats.Add(new PtpChat
                        {
                            FirstUserId = ptpChat.FirstUserId,
                            Id = ptpChat.Id,
                            SecondUserId = ptpChat.SecondUser.Id
                        });
                    }                    
                    break;
                case ChatTypes.GroupChat:
                    var groupChat = (GroupChat)chat;
                    foreach (var user in groupChat.Users)
                    {
                        if (!db.Users.Contains(user))
                        {
                            db.Users.Add(user);
                        }
                    }
                    db.GroupChats.Add(groupChat);
                    break;
            }
            await db.SaveChangesAsync();
        }

        public List<PtpChat> GetPtpChats()
        {
            using var db = new DatabaseContext();
            return db.PtpChats
                .Include(x => x.SecondUser)
                .ToList();
        }

        public List<GroupChat> GetGroupChats()
        {
            using var db = new DatabaseContext();
            return db.GroupChats.ToList();
        }

        public bool PtpChatIsExists(Guid idChat)
        {
            using var db = new DatabaseContext();
            var chat = db.PtpChats.Find(idChat);
            return chat != null;
        }

        public bool GroupChatIsExists(Guid idChat)
        {
            using var db = new DatabaseContext();
            var chat = db.GroupChats.Find(idChat);
            return chat != null;
        }

        public User GetSenderUserByGroupChatId(Guid chatId, Guid senderId)
        {
            using var db = new DatabaseContext();
            var chat = db.GroupChats.Find(chatId);
            foreach (var user in chat.Users)
            {
                if (user.Id == senderId)
                {
                    return user;
                }
            }
            return null;
        }

        public async Task<PtpChat> GetPtpChatById(Guid id)
        {
            using var db = new DatabaseContext();
            var result = await db.PtpChats
                .Include(x => x.SecondUser)
                .FirstOrDefaultAsync(x => x.Id == id);
            while (result is null)
            {
                await Task.Delay(10);
                result = await db.PtpChats
                .Include(x => x.SecondUser)
                .FirstOrDefaultAsync(x => x.Id == id);
            }
            return result;
        }

        public async Task<GroupChat> GetGroupChatById(Guid id)
        {
            using var db = new DatabaseContext();
            var result = await db.GroupChats
                .FirstOrDefaultAsync(x => x.Id == id);
            while (result is null)
            {
                await Task.Delay(10);
                result = await db.GroupChats
                .FirstOrDefaultAsync(x => x.Id == id);
            }
            return result;
        }

        public int GetCountNotReadMessageByChatId(Guid chatId)
        {
            using var db = new DatabaseContext();
            var myId = db.Users.First().Id;
            return db.MessageStatus
                .Include(x => x.Message)
                .Where(x => x.Message.ChatId == chatId && x.Status == MessageStatuses.Sent && x.RecipientId == myId)
                .ToList().Count;
        }

        public void ReadAllNonMyMessages(Guid chatId)
        {
            using var db = new DatabaseContext();
            var myId = db.Users.First().Id;
            var messages = db.MessageStatus
                .Include(x => x.Message)
                .Where(x => x.Message.ChatId == chatId && x.Status == MessageStatuses.Sent && x.RecipientId == myId)
                .ToList();
            foreach(var message in messages)
            {
                message.Status = MessageStatuses.Read;
            }
            db.SaveChanges();
        }
    }
}
