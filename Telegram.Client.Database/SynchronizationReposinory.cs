﻿using Microsoft.EntityFrameworkCore;
using Telegram.Client.Db.Context;
using Telegram.Client.Db.Context.Entity;
using Telegram.Core.Services.Models.Messages;
using Telegram.Core.Services.Models.Requests;

namespace Telegram.Client.Database
{
    public class SynchronizationReposinory
    {
        private readonly ChatRepository _chatRepository = new ChatRepository();
        private readonly MessageRepository _messageRepository = new MessageRepository();
        public async Task Synchronization(SynchronizationModelResponse model)
        {
            using var db = new DatabaseContext();
            if (model.PtpChats.Count > 0)
            {
                foreach(var chat in model.PtpChats)
                {
                    var secondUser = db.Users.FirstOrDefault(x => x.Id == chat.UserId);
                    if (secondUser == null)
                    {
                        secondUser = new User
                        {
                            Id = chat.UserId,
                            Login = chat.Login,
                            Username = chat.Username
                        };
                        db.Users.Add(secondUser);
                        db.SaveChanges();
                    }
                    await _chatRepository.SaveChat(new PtpChat
                    {
                        Id = chat.ChatId,
                        FirstUserId = db.Users.First().Id,
                        SecondUser = secondUser
                    });
                }
            }
            db.SaveChanges();
            if (model.GroupChats.Count > 0)
            {
                foreach(var chat in model.GroupChats)
                {
                    var users = new List<User>();
                    foreach (var user in chat.Users)
                    {
                        users.Add(new User
                        {
                            Id = user.Id,
                            Username = user.Name,
                            Login = user.Login,
                        });
                    }
                    await _chatRepository.SaveChat(new GroupChat
                    {
                        Id = chat.ChatId,
                        NameChat = chat.NameChat,
                        Users = users
                    });
                }
            }
            db.SaveChanges();
            if (model.Messages.Count > 0)
            {
                foreach(var message in model.Messages)
                {
                    var ptpChat = db.PtpChats.FirstOrDefault(x => x.Id == message.ChatId);
                    var groupChat = db.GroupChats.FirstOrDefault(x => x.Id == message.ChatId);
                    if (ptpChat != null || groupChat != null)
                    {
                        if (message.FileId == Guid.Empty)
                        {
                            _messageRepository.SaveSynchronizedPtpMesage(new PlainMessageDto
                            {
                                Chat = new PtpChatDto
                                {
                                    Id = message.ChatId
                                },
                                Id = message.MessageId,
                                Content = message.Content,
                                Sender = message.SenderId,
                                Reply = message.Reply,
                            }, message.DateOfReceiving);
                        }
                        else
                        {
                            _messageRepository.SaveSynchronizedMediaMesage(new MediaMessageDto
                            {
                                Chat = new PtpChatDto
                                {
                                    Id = message.ChatId
                                },
                                Id = message.MessageId,
                                Content = message.Content,
                                Sender = message.SenderId,
                                Reply = message.Reply,
                                FileId= message.FileId,
                                MediaType= message.MediaType
                            }, message.DateOfReceiving);
                        }                      
                        var wasReceived = await db.MessageStatus.FirstOrDefaultAsync(x => x.MessageId == message.MessageId);
                        wasReceived.Status = MessageStatuses.Sent;
                        _messageRepository.MessageRead(message.ChatId);
                    }                       
                }
            }
            db.SaveChanges();
        }

        public SynchronizationModelRequest GetSynchronizationModel()
        {
            using var db = new DatabaseContext();
            var result = new SynchronizationModelRequest();
            var ptpChats = db.PtpChats.ToList();
            var groupChats = db.GroupChats.ToList();
            var messages = db.Messages.ToList();
            foreach(var chat in ptpChats)
            {
                result.PtpChatsId.Add(chat.Id);
            }
            foreach(var chat in groupChats)
            {
                result.GroupChatsId.Add(chat.Id);
            }
            foreach(var message in messages)
            {
                result.MessagesId.Add(message.Id);
            }
            result.UserId = db.Users.First().Id;
            return result;
        }
    }
}
