﻿using Telegram.Client.Db.Context;
using Telegram.Client.Db.Context.Entity;

namespace Telegram.Client.Database
{
    public class UserRepository
    {
        public async Task SaveUserInformation (User user)
        {            
            using var db = new DatabaseContext();
            await db.Users.AddAsync(user);
            await db.SaveChangesAsync();
        }

        public async Task<bool> ExistsUser (Guid idUser)
        {
            using var db = new DatabaseContext();
            var user = await db.Users.FindAsync(idUser);
            return user != null;
        } 

        public User GetUser()
        {
            using var db = new DatabaseContext();
            return db.Users.FirstOrDefault();
        }

        public string GetToken()
        {
            using var db = new DatabaseContext();
            var user = db.Users.FirstOrDefault();
            if (user is null)
            {
                return null;
            }
            return user.Token;
        }
    }
}