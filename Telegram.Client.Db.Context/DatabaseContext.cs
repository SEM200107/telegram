﻿using Microsoft.EntityFrameworkCore;
using Telegram.Client.Db.Context.Entity;

namespace Telegram.Client.Db.Context
{
    public class DatabaseContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<PtpChat> PtpChats { get; set; }
        public DbSet<GroupChat> GroupChats { get; set; }
        public DbSet<MessageStatus> MessageStatus { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=database.db");
    }
}