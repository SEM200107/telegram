﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Client.Db.Context.Entity
{
    public class Message : BaseEntity
    {
        public Guid ChatId { get; set; }
        public User Sender { get; set; }
        public Guid SenderId { get; set; } 
        public string Content { get; set; }
        public Guid FileId { get; set; }
        public MediaTypes MediaType { get; set; }
        public Guid Reply { get; set; }
        public DateTime DateOfReceiving { get; set; }
    }
}
