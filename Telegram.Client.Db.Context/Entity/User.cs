﻿namespace Telegram.Client.Db.Context.Entity
{
    public class User : BaseEntity
    {        
        public string Token { get; set; }
        public string Username { get; set; }
        public string Login { get; set; }
    }
}
