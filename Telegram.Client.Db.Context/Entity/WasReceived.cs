﻿namespace Telegram.Client.Db.Context.Entity
{
    public class MessageStatus : BaseEntity
    {
        public Guid SenderId { get; set; }
        public Guid RecipientId { get; set; }
        public Message Message { get; set; }
        public Guid MessageId { get; set; }
        public MessageStatuses Status { get; set; } = MessageStatuses.NotSent;
    }
}
