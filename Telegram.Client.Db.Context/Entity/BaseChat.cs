﻿using System.ComponentModel.DataAnnotations;
using Telegram.Core.Services.Models.Enums;

namespace Telegram.Client.Db.Context.Entity
{
    public abstract class BaseChat
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
        public abstract ChatTypes ChatTypes { get; }

    }
}
