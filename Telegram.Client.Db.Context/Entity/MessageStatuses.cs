﻿namespace Telegram.Client.Db.Context.Entity
{
    public enum MessageStatuses
    {
        NotSent = 1,
        Sent = 2,
        Read = 3
    }
}
