﻿using System.ComponentModel.DataAnnotations;

namespace Telegram.Client.Db.Context.Entity
{
    public class BaseEntity
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();
        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
    }
}
