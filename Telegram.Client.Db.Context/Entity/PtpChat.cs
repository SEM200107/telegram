﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Client.Db.Context.Entity
{
    public class PtpChat : BaseChat
    {
        public Guid FirstUserId { get; set; }
        public User SecondUser { get; set; }
        public Guid SecondUserId { get; set; }
        public override ChatTypes ChatTypes => ChatTypes.PtpChat;
    }
}
