﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Db.Context.Entity
{
    public class Message : BaseEntity
    {       
        public BaseChat Chat { get; set; }
        public Guid ChatId { get; set; }
        public User Sender { get; set; }
        public Guid SenderId { get; set; }
        public string Content { get; set; }
        public Guid FileId { get; set; }
        public MediaTypes MediaType { get; set; }
        public Guid Reply { get; set; }
        public bool IsDeleted { get; set; } = false;
    }
}
