﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Db.Context.Entity
{
    public class PtpChat : BaseChat
    {
        public User FirstUser { get; set; }
        public Guid FirstUserId { get; set; }

        public User SecondUser { get; set; }
        public Guid SecondUserId { get; set; }

        public override ChatTypes ChatTypes => ChatTypes.PtpChat;
    }
}
