﻿namespace Telegram.Core.Db.Context.Entity
{
    public class User : BaseEntity
    {        
        public string Username { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
