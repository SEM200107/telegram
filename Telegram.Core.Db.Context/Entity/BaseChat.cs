﻿using System.ComponentModel.DataAnnotations;
using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Db.Context.Entity
{
    public abstract class BaseChat
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();
        public abstract ChatTypes ChatTypes { get; }
        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
    }
}
