﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;
using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Db.Context.Entity
{
    public class GroupChat : BaseChat
    {
        public string NameChat { get; set; }
        public string StringUsers { get; set; }
        [NotMapped]
        public List<User> Users
        {
            get { return JsonSerializer.Deserialize<List<User>>(StringUsers); }
            set { StringUsers = JsonSerializer.Serialize(value); }
        }
        public override ChatTypes ChatTypes => ChatTypes.GroupChat;
    }
}
