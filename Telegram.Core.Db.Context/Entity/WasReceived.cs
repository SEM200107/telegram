﻿namespace Telegram.Core.Db.Context.Entity
{
    public class WasReceived : BaseEntity
    {
        public User Sender { get; set; }
        public Guid SenderId { get; set; }
        public User Recipient { get; set; }
        public Guid RecipientId { get; set; }
        public Message Message { get; set; }
        public Guid MessageId { get; set; }
        public bool IsReceived { get; set; } = false;
        public bool IsDeleted { get; set; } = false;
    }
}
