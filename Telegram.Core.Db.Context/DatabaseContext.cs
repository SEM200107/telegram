﻿using Microsoft.EntityFrameworkCore;
using Telegram.Core.Db.Context.Entity;

namespace Telegram.Core.Db.Context
{
    public class DatabaseContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<GroupChat> GroupChats { get; set; }
        public DbSet<PtpChat> PtpChats { get; set; }
        public DbSet<WasReceived> WasReceiveds { get; set; }

        public DatabaseContext(DbContextOptions<DbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public DatabaseContext(string conectionString)
            : base(GetOptions(conectionString))
        {
            Database.EnsureCreated();
        }

        private static DbContextOptions GetOptions(string conectionString)
        {
            return NpgsqlDbContextOptionsBuilderExtensions.UseNpgsql(new DbContextOptionsBuilder(), conectionString).Options;
        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.HasDefaultSchema("public");

        //    modelBuilder.Entity<Message>().ToTable("messages");

        //    modelBuilder.Entity<User>().ToTable("users");
        //    modelBuilder.Entity<User>().Property(e => e.Id);
        //    modelBuilder.Entity<User>().HasMany(e => e.Messages).WithOne(e => e.Sender).HasForeignKey(e => e.IdSender).OnDelete(DeleteBehavior.Cascade);
        //    modelBuilder.Entity<User>().HasMany(e => e.Messages).WithOne(e => e.Recipient).HasForeignKey(e => e.IdRecipient).OnDelete(DeleteBehavior.Cascade);
        //}
    }
}