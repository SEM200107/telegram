﻿using System.Threading.Tasks;

namespace Telegram.Client.Gui.ViewModels
{
    public class BasePageViewModel : ViewModelBase
    {
        public virtual void SetContext(object context)
        {
        }
        public virtual void ReloadPage()
        {
        }

        public virtual async Task<bool?> DialogResult()
        {
            return null;
        }
    }
}
