﻿using ReactiveUI;
using System.Windows.Input;
using Telegram.Client.Gui.Services;

namespace Telegram.Client.Gui.ViewModels
{
    public class ImageFullSizeViewModel : BasePageViewModel
    {
        public ICommand GoBackCommand => ReactiveCommand.Create(GoBack);
        public string Path { get; set; }

        private readonly INavigationService _navigationService;

        public ImageFullSizeViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public override void SetContext(object context)
        {
            var path = (string)context;
            Path = path;
        }

        private void GoBack()
        {
            _navigationService.NavigateBack();
        }
    }
}
