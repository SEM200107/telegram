﻿using DynamicData;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Telegram.Client.Core;
using Telegram.Client.Database;
using Telegram.Client.Gui.Models;
using Telegram.Client.Gui.Models.Chats;
using Telegram.Client.Gui.Services;
using Telegram.Core.Services.Models.Enums;
using Telegram.Core.Services.Models.Messages;

namespace Telegram.Client.Gui.ViewModels
{
    public class ChatSelectionViewModel : BasePageViewModel
    {
        public ICommand SearchUserCommand => ReactiveCommand.Create(ProcessSearchUser);
        public ICommand ExitProfileCommand => ReactiveCommand.Create(ProcessExitProfile);
        public ICommand CreateGroupChatCommand => ReactiveCommand.Create(ProcessCreateGroupChat);
        public ICommand ChatSelectedCommand => ReactiveCommand.Create<BaseChatModel>(ProcessSelectedChat);
        public string SearchUsername
        {
            get => _search;
            set => this.RaiseAndSetIfChanged(ref _search, value);
        }
        public ObservableCollection<BaseChatModel> ChatList
        {
            get => _chatList;
            set => this.RaiseAndSetIfChanged(ref _chatList, value);
        }

        private string _search;
        private ObservableCollection<BaseChatModel> _chatList;
        private readonly INavigationService _navigationService;
        private readonly LocalStorage _localStorage;
        private readonly ServerInteractionService _serverInteractionService;
        private readonly ChatService _chatService;
        private readonly ChatRepository _chatRepository;

#if DEBUG
        public ChatSelectionViewModel()
        {
            ChatList = new ObservableCollection<BaseChatModel>
            {
                new GroupChatModel
                {
                    NameChat = "Preview"
                },
                new PtpChatModel
                {
                    Login = "Preview User Login",
                    Username = "Preview Username"
                }
            };
        }
#endif

        public ChatSelectionViewModel(INavigationService navigationService,
            LocalStorage localStorage,
            ServerInteractionService serverInteractionService,
            ChatService chatService)
        {
            _navigationService = navigationService;
            _localStorage = localStorage;
            _serverInteractionService = serverInteractionService;
            _chatService = chatService;
            _chatRepository = new ChatRepository();
            _chatList = new ObservableCollection<BaseChatModel>();
            _serverInteractionService.MessageObservable.Subscribe(ProcessChat);
            Start();
        }

        public override void ReloadPage()
        {
            Start();
        }

        private void Start()
        {
            ChatList.Clear();
            var chatList = _chatRepository.GetPtpChats();
            var ptpChatModelList = new List<PtpChatModel>();
            foreach (var chat in chatList)
            {
                var model = new PtpChatModel
                {
                    ChatId = chat.Id,
                    Login = chat.SecondUser.Login,
                    UserId = chat.SecondUser.Id,
                    Username = chat.SecondUser.Username,
                };
                var countNotReadMessage = _chatRepository.GetCountNotReadMessageByChatId(chat.Id);
                if (countNotReadMessage > 0)
                {
                    model.CountNewMessage = countNotReadMessage;
                    model.NewMessage = true;
                }
                ptpChatModelList.Add(model);
            }

            ChatList.AddRange(ptpChatModelList);
            var groupChatList = _chatRepository.GetGroupChats();
            foreach (var chat in groupChatList)
            {
                var usersId = new List<Guid>();
                foreach (var user in chat.Users)
                {
                    usersId.Add(user.Id);
                }
                var model = new GroupChatModel
                {
                    ChatId = chat.Id,
                    NameChat = chat.NameChat,
                    UsersId = usersId
                };
                var countNotReadMessage = _chatRepository.GetCountNotReadMessageByChatId(chat.Id);
                if (countNotReadMessage > 0)
                {
                    model.CountNewMessage = countNotReadMessage;
                    model.NewMessage = true;
                }
                ChatList.Add(model);
            }
            foreach (var chat in ChatList)
            {
                _localStorage.Chats.Add(chat.ChatId);
            }
        }

        private async void ProcessSearchUser()
        {
            if (SearchUsername is null)
            {
                Start();
                return;
            }
            ChatList.Clear();
            var ptpChatModels = await _chatService.SearchUserPtpChat(SearchUsername);
            ChatList.AddRange(ptpChatModels);
            SearchUsername = null;
        }

        private async void ProcessChat(BaseProtocolDto message)
        {
            switch (message.MessageType)
            {
                case MessageTypes.PlainMassage:
                case MessageTypes.MediaMessage:
                    if (!_localStorage.Chats.Contains(message.Chat.Id))
                    {
                        _localStorage.Chats.Add(message.Chat.Id);
                        switch (message.Chat.ChatTypes)
                        {
                            case ChatTypes.PtpChat:
                                var ptpChat = await _chatRepository.GetPtpChatById(message.Chat.Id);
                                ChatList.Add(new PtpChatModel
                                {
                                    ChatId = ptpChat.Id,
                                    Login = ptpChat.SecondUser.Login,
                                    UserId = ptpChat.SecondUser.Id,
                                    Username = ptpChat.SecondUser.Username
                                });
                                break;
                            case ChatTypes.GroupChat:
                                var groupChat = await _chatRepository.GetGroupChatById(message.Chat.Id);
                                var groupChatModel = new GroupChatModel
                                {
                                    ChatId = groupChat.Id,
                                    NameChat = groupChat.NameChat,
                                    UsersId = new List<Guid>()
                                };
                                foreach (var user in groupChat.Users)
                                {
                                    groupChatModel.UsersId.Add(user.Id);
                                }
                                ChatList.Add(groupChatModel);
                                break;
                        }
                    }
                    foreach (var chat in ChatList)
                    {
                        if (chat.ChatId == message.Chat.Id)
                        {
                            ChatList.Remove(chat);
                            chat.CountNewMessage++;
                            chat.NewMessage = true;
                            ChatList.Add(chat);
                            break;
                        }
                    }
                    break;
            }
        }

        private void ProcessSelectedChat(BaseChatModel selected)
        {
            _navigationService.NavigateTo<ChatViewModel>(selected);
        }

        private void ProcessCreateGroupChat()
        {
            var ptpChats = new ObservableCollection<PtpChatModel>();
            foreach(var chat in ChatList)
            {
                if (chat.ChatType == ChatTypes.PtpChat)
                {
                    ptpChats.Add((PtpChatModel)chat);
                }
            }
            _navigationService.NavigateTo<CreatingGroupChatViewModel>(ptpChats);
        }

        private void ProcessExitProfile()
        {
            _serverInteractionService.StopConnection();
            _navigationService.NavigateBack();
        }
    }
}