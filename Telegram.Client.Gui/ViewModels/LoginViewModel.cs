﻿using ReactiveUI;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Input;
using Telegram.Client.Core;
using Telegram.Client.Database;
using Telegram.Client.Db.Context.Entity;
using Telegram.Client.Gui.Models;
using Telegram.Client.Gui.Services;
using Telegram.Core.Services.Models;
using Telegram.Core.Services.Models.Requests;

namespace Telegram.Client.Gui.ViewModels
{
    public class LoginViewModel : BasePageViewModel
    {
        public ICommand NavigateToRegistrationCommand => ReactiveCommand.Create(ProcessNavigateToRegestration);
        public ICommand LoginCommand => ReactiveCommand.Create(ProcessLogin);
        public string Login { get; set; }
        public string Password { get; set; }
        public bool Error
        {
            get => _error;
            set => this.RaiseAndSetIfChanged(ref _error, value);
        }
        public string ErrorText
        {
            get => _errorText;
            set => this.RaiseAndSetIfChanged(ref _errorText, value);
        }

        private string _errorText;
        private bool _error;
        private readonly INavigationService _navigationService;
        private readonly ServerInteractionService _serverInteractionService;
        private readonly LocalStorage _localStorage;
        private readonly UserRepository _userRepository;
        private readonly RequestService _requestService;

        public LoginViewModel(INavigationService navigationService,
            ServerInteractionService serverInteractionService,
            LocalStorage localStorage)
        {
            _navigationService = navigationService;
            _serverInteractionService = serverInteractionService;
            _localStorage = localStorage;
            Error = false;
            _requestService = new RequestService();
            _userRepository = new UserRepository();
        }

        public void ProcessNavigateToRegestration()
        {
            _navigationService.NavigateTo<RegistrationViewModel>();
        }

        public async void ProcessLogin()
        {
            var model = new LoginDto
            {
                Login = Login,
                Password = Encoding.UTF8.GetBytes(Password)
            };
            var content = await _requestService.SendPostRequest(model, $"http://{Constants.Ip}/account/login");
            var response = JsonSerializer.Deserialize<LoginResponseModel>
                (content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new LoginResponseModel();
            if (response.Error != null)
            {
                ErrorText = response.Error;
                Error = true;
                return;
            }
            var user = new User
            {
                Id = response.Id,
                Token = response.Token,
                Username = response.Username,
                Login = response.Login,
            };
            if (!await _userRepository.ExistsUser(user.Id))
            {
                await _userRepository.SaveUserInformation(user);
            }
            SaveLocalStorage(response);
            if (await Connect())
            {
                _navigationService.NavigateTo<ChatSelectionViewModel>();
            }
            Error = false;
        }

        private void SaveLocalStorage(LoginResponseModel model)
        {
            _localStorage.Id = model.Id;
            _localStorage.Token = model.Token;
            _localStorage.Username = model.Username;
        }

        private async Task<bool> Connect()
        {
            var connectService = new ConnectionService(_serverInteractionService);
            return await connectService.ConnectToServer(_localStorage.Id, _localStorage.Token);
        }
    }
}
