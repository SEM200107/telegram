﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.Json;
using System.Windows.Input;
using Telegram.Client.Core;
using Telegram.Client.Database;
using Telegram.Client.Db.Context.Entity;
using Telegram.Client.Gui.Models;
using Telegram.Client.Gui.Models.Chats;
using Telegram.Client.Gui.Services;
using Telegram.Core.Services.Models;
using Telegram.Core.Services.Models.Enums;
using Telegram.Core.Services.Models.Messages;
using Telegram.Core.Services.Models.Requests;

namespace Telegram.Client.Gui.ViewModels
{
    public class CreatingGroupChatViewModel : BasePageViewModel
    {
        public ICommand SearchUserCommand => ReactiveCommand.Create(ProcessSearchUser);
        public ICommand AddUserCommand => ReactiveCommand.Create(ProcessAddUserUser);
        public ICommand CreateGroupChatCommand => ReactiveCommand.Create(ProcessCreateGroupChat);
        public ICommand BackCommand => ReactiveCommand.Create(ProcessBack);
        public PtpChatModel SelectUser { get; set; }
        
        public ObservableCollection<PtpChatModel> SearchList
        {
            get => _searchList;
            set => this.RaiseAndSetIfChanged(ref _searchList, value);
        }
        public ObservableCollection<PtpChatModel> Users
        {
            get => _users;
            set => this.RaiseAndSetIfChanged(ref _users, value);
        }
        public string SearchUsername { get; set; }
        public string NameChat { get; set; }

        private ObservableCollection<PtpChatModel> _searchList;
        private ObservableCollection<PtpChatModel> _users;
        private readonly INavigationService _navigationService;
        private readonly LocalStorage _localStorage;
        private readonly RequestService _requestService;
        private readonly ChatRepository _chatRepository;

        public CreatingGroupChatViewModel(INavigationService navigationService, LocalStorage localStorage)
        {
            _navigationService = navigationService;
            _localStorage = localStorage;
            _searchList = new ObservableCollection<PtpChatModel>();
            _users = new ObservableCollection<PtpChatModel>();
            _requestService = new RequestService();
            _chatRepository = new ChatRepository();
        }

        public override void SetContext(object context)
        {
            SearchList = (ObservableCollection<PtpChatModel>)context;            
        }
        private async void ProcessSearchUser()
        {
            var chat = new SearchChatDto
            {
                SearchString = SearchUsername,
                UserId = _localStorage.Id
            };
            var content = await _requestService.SendPostRequest(chat, $"http://{Constants.Ip}/chat/searchchat");
            var response = JsonSerializer.Deserialize<ObservableCollection<PtpChatModel>>
                (content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new ObservableCollection<PtpChatModel>();
            SearchList = response;
        }

        private void ProcessAddUserUser()
        {
            Users.Add(SelectUser);
            SearchList.Remove(SelectUser);
        }

        public async void ProcessCreateGroupChat()
        {
            var creatingChat = new CreatingChatDto
            {
                NameChat = NameChat,
                ChatTypes = ChatTypes.GroupChat,
                UsersId = new List<Guid>()
            };
            var users = new List<User>();
            users.Add(new User
            {
                Id = _localStorage.Id,
                Username = _localStorage.Username,
                Token = _localStorage.Token
            });
            creatingChat.UsersId.Add(_localStorage.Id);
            foreach (var user in Users)
            {
                users.Add(new User
                {
                    Id = user.UserId,
                    Username = user.Username
                });
                creatingChat.UsersId.Add(user.UserId);
            }
            var content = await _requestService.SendPostRequest(creatingChat, $"http://{Constants.Ip}/chat/creatingchat");
            var chatId = JsonSerializer.Deserialize<Guid>(content);
            await _chatRepository.SaveChat(new GroupChat
            {
                Id = chatId,
                NameChat = NameChat,
                Users = users
            });
            _navigationService.NavigateBack(true);
        }
        
        private void ProcessBack()
        {
            _navigationService.NavigateBack();
        }
    }
}
