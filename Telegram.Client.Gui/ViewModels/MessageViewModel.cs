﻿using ReactiveUI;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Input;
using Telegram.Client.Database.Models;
using Telegram.Client.Db.Context.Entity;
using Telegram.Client.Gui.Models;
using Telegram.Client.Gui.Services;
using Telegram.Core.Services.Models;
using Telegram.Core.Services.Models.Enums;
using MessageModel = Telegram.Client.Gui.Models.MessageModel;

namespace Telegram.Client.Gui.ViewModels
{
    public class MessageViewModel : ViewModelBase
    {
        public ICommand OpenPhotoCommand => ReactiveCommand.Create(OpenPhoto);
        public ICommand OpenFileCommand => ReactiveCommand.Create(OpenFile);
        public string Content { get; set; }
        public bool IsOwn { get; set; }
        public bool PhotoMessage
        {
            get => _photoMessage;
            set => this.RaiseAndSetIfChanged(ref _photoMessage, value);
        }
        public bool FileMessage
        {
            get => _fileMessage;
            set => this.RaiseAndSetIfChanged(ref _fileMessage, value);
        }
        public string SenderName { get; set; }
        public string Time { get; set; }
        public string Icon
        {
            get => _icon;
            set => this.RaiseAndSetIfChanged(ref _icon, value);
        }
        public string PhotoPath
        {
            get => _photoPath;
            set => this.RaiseAndSetIfChanged(ref _photoPath, value);
        }
        public bool LoadingSpinner
        {
            get => _loadingSpinner;
            set => this.RaiseAndSetIfChanged(ref _loadingSpinner, value);
        }
        public Guid Id;
        public Models.ReplyMessage Reply
        {
            get => _replyMessage;
            set => this.RaiseAndSetIfChanged(ref _replyMessage, value);
        }
        public bool ReplyMessage
        {
            get => _replayMessage;
            set => this.RaiseAndSetIfChanged(ref _replayMessage, value);
        }

        private bool _replayMessage;
        private Models.ReplyMessage _replyMessage;
        private string _icon;
        private string _photoPath;
        private string _filePath;
        private bool _loadingSpinner;
        private bool _photoMessage;
        private bool _fileMessage;
        private readonly INavigationService _navigationService;

        public MessageViewModel()
        {
        }
        public MessageViewModel(LocalStorage localStorage, MessageModel messageModel, INavigationService navigationService)
        {
            _navigationService = navigationService;
            LoadingSpinner = false;
            PhotoMessage = false;
            FileMessage = false;
            ReplyMessage = false;
            Id = messageModel.MessageId;
            Content = messageModel.Content;
            IsOwn = localStorage.Id == messageModel.SenderId;
            Time = messageModel.Time;
            switch (messageModel.Status)
            {
                case MessageStatuses.NotSent:
                    Icon = "ClockOutline";
                    break;
                case MessageStatuses.Sent:
                    Icon = "Check";
                    break;
                case MessageStatuses.Read:
                    Icon = "CheckAll";
                    break;
            }
            if (messageModel.SenderName != null && !IsOwn)
            {
                SenderName = messageModel.SenderName;
            }
            if (messageModel.File != null)
            {
                ProcessMediaMessage(messageModel.File);
            }
            if (messageModel.ReplyMessage != null)
            {
                Reply = messageModel.ReplyMessage;
                ReplyMessage = true;
            }
        }

        private async void ProcessMediaMessage(FileIdInMessage media)
        {
            if (!Directory.Exists("Download"))
            {
                Directory.CreateDirectory("Download");
            }
            var path = Environment.CurrentDirectory;
            switch (media.MediaType)
            {
                case MediaTypes.PhotoMessage:
                    if (!Directory.Exists("Download\\Photos"))
                    {
                        Directory.CreateDirectory("Download\\Photos");
                    }
                    if (!File.Exists($"{path}\\Download\\Photos\\{media.FileId}.png"))
                    {
                        LoadingSpinner = true;
                        await DownloadPhoto(media.FileId);
                        LoadingSpinner = false;
                    }
                    PhotoPath = $"{path}\\Download\\Photos\\{media.FileId}.png";
                    PhotoMessage = true;
                    break;
                case MediaTypes.FileMessage:
                    if (!Directory.Exists("Download\\Files"))
                    {
                        Directory.CreateDirectory("Download\\Files");
                    }
                    if (!File.Exists($"{path}\\Download\\Files\\{media.FileId}.rar"))
                    {
                        LoadingSpinner = true;
                        await DownloadFile(media.FileId);
                        LoadingSpinner = false;
                    }
                    _filePath = $"{path}\\Download\\Files\\{media.FileId}.rar";
                    FileMessage = true;
                    break;
            }
        }

        private async Task DownloadPhoto(Guid mediaId)
        {
            var request = new HttpRequestMessage()
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"http://{Constants.Ip}/chat/downloadphoto?mediaId={mediaId}"),
            };
            var httpClient = new HttpClient();
            var response = await httpClient.SendAsync(request);
            var content = await response.Content.ReadAsStringAsync();
            var bytesFile = JsonSerializer.Deserialize<byte[]>(content);
            File.WriteAllBytes($"Download\\Photos\\{mediaId}.png", bytesFile);
        }

        private async Task DownloadFile(Guid mediaId)
        {
            var request = new HttpRequestMessage()
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"http://{Constants.Ip}/chat/downloadfile?mediaId={mediaId}"),
            };
            var httpClient = new HttpClient();
            var response = httpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead).GetAwaiter().GetResult();
            await using var content = await response.Content.ReadAsStreamAsync();
            await using var fileStream = File.OpenWrite($"Download\\Files\\{mediaId}.rar");
            await content.CopyToAsync(fileStream);
        }

        private void OpenPhoto()
        {
            _navigationService.NavigateTo<ImageFullSizeViewModel>(PhotoPath);
        }

        private void OpenFile()
        {
            Process.Start(new ProcessStartInfo
            {
                FileName = "explorer",
                Arguments = $"/n, /select, {_filePath}"
            });
        }
    }
}
