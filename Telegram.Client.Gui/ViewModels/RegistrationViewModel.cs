﻿using ReactiveUI;
using System.Text;
using System.Text.Json;
using System.Windows.Input;
using Telegram.Client.Core;
using Telegram.Client.Gui.Services;
using Telegram.Core.Services.Models;
using Telegram.Core.Services.Models.Requests;

namespace Telegram.Client.Gui.ViewModels
{
    public class RegistrationViewModel : BasePageViewModel
    {
        public ICommand RegisterCommand => ReactiveCommand.Create(ProcessRegistration);
        public ICommand BackCommand => ReactiveCommand.Create(GoBack);
        public string Username { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool Error
        {
            get => _error;
            set => this.RaiseAndSetIfChanged(ref _error, value);
        }
        public string ErrorText
        {
            get => _errorText;
            set => this.RaiseAndSetIfChanged(ref _errorText, value);
        }

        private string _errorText;
        private bool _error;
        private readonly INavigationService _navigationService;
        private readonly RequestService _requestService = new RequestService();

        public RegistrationViewModel(INavigationService navigationService)
        {
            Error = false;
            _navigationService = navigationService;
        }
        private async void ProcessRegistration()
        {
            if (Username is null || Login is null || Password is null)
            {
                ErrorText = "Заполнены не все поля!";
                Error = true;
                return;
            }
            if (Password.Length < 6)
            {
                ErrorText = "Слишком короткий пароль!";
                Error = true;
                return;
            }
            foreach(var c in Password)
            {
                if (!(char.IsNumber(c) || c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z'))
                {
                    ErrorText = "Неверный формат пароля!";
                    Error = true;
                    return;
                }                
            }
            var model = new RegistrationDto
            {
                Username = Username,
                Login = Login,
                Password = Encoding.UTF8.GetBytes(Password)
            };
            var response = JsonSerializer.Deserialize<bool>
                (await _requestService.SendPostRequest(model, $"http://{Constants.Ip}/account/registration"));
            if (!response)
            {
                ErrorText = "Этот логин уже занят!";
                Error = true;
                return;
            }
            _navigationService.NavigateBack();
        }

        private void GoBack()
        {
            _navigationService.NavigateBack();
        }
    }
}
