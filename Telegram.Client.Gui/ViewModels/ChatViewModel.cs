﻿using Avalonia.Controls;
using ReactiveUI;
using SkiaSharp;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using Telegram.Client.Core;
using Telegram.Client.Database;
using Telegram.Client.Db.Context.Entity;
using Telegram.Client.Gui.Models;
using Telegram.Client.Gui.Models.Chats;
using Telegram.Client.Gui.Services;
using Telegram.Core.Services.Models.Enums;
using Telegram.Core.Services.Models.Messages;

namespace Telegram.Client.Gui.ViewModels
{
    public class ChatViewModel : BasePageViewModel
    {
        #region fields and properties
        public ICommand SendCommand => ReactiveCommand.Create(SendingMessage);
        public ICommand RemoveMediaCommand => ReactiveCommand.Create(ProcessRemoveMedia);
        public ICommand BackCommand => ReactiveCommand.Create(GoBack);
        public ICommand DeleteMessageCommand => ReactiveCommand.Create(ProcessDeletedMessage);
        public ICommand ImageCommand => ReactiveCommand.Create(ProcessImage);
        public ICommand FileCommand => ReactiveCommand.Create(ProcessFile);
        public ICommand ReplyMessageCommand => ReactiveCommand.Create(ProcessReplyMessage);
        public string Content
        {
            get => _content;
            set => this.RaiseAndSetIfChanged(ref _content, value);
        }
        public string Username
        {
            get => _username;
            set => this.RaiseAndSetIfChanged(ref _username, value);
        }
        public string NameChat
        {
            get => _nameChat;
            set => this.RaiseAndSetIfChanged(ref _nameChat, value);
        }
        public ObservableCollection<MessageViewModel> Message
        {
            get => _message;
            set => this.RaiseAndSetIfChanged(ref _message, value);
        }
        public MessageViewModel SelectedMessage { get; set; }
        public DeletedMessageDialogViewModel Overlay
        {
            get => _overlay;
            set => this.RaiseAndSetIfChanged(ref _overlay, value);
        }
        public string FilePath
        {
            get => _filePath;
            set => this.RaiseAndSetIfChanged(ref _filePath, value);
        }
        public bool PreviewPhoto
        {
            get => _previewPhoto;
            set => this.RaiseAndSetIfChanged(ref _previewPhoto, value);
        }
        public bool PreviewFile
        {
            get => _previewFile;
            set => this.RaiseAndSetIfChanged(ref _previewFile, value);
        }
        public ReplyMessage Reply
        {
            get => _replyMessage;
            set => this.RaiseAndSetIfChanged(ref _replyMessage, value);
        }
        public bool ReplyMessage
        {
            get => _replayMessage;
            set => this.RaiseAndSetIfChanged(ref _replayMessage, value);
        }
        public bool LoadingSpinner
        {
            get => _loadingSpinner;
            set => this.RaiseAndSetIfChanged(ref _loadingSpinner, value);
        }

        private bool _loadingSpinner;
        private bool _replayMessage;
        private ReplyMessage _replyMessage;
        private bool _previewPhoto;
        private bool _previewFile;
        private string _filePath;
        private string _username;
        private string _nameChat;
        private string _content;
        private DeletedMessageDialogViewModel _overlay;
        private ObservableCollection<MessageViewModel> _message;
        private BaseChatModel _chatModel;
        private readonly ServerInteractionService _serverInteractionService;
        private readonly LocalStorage _localStorage;
        private readonly INavigationService _navigationService;
        private readonly MessageRepository _messageRepository;
        private readonly ChatRepository _chatRepository;
        private readonly ChatService _chatService;
        private readonly OverlayService _overlayService;
        private FileInMessage _fileInMessage;        
        #endregion

        public ChatViewModel(ServerInteractionService serverInteractionService, LocalStorage localStorage,
            INavigationService navigationService, ChatService chatService, OverlayService overlayService)
        {
            _serverInteractionService = serverInteractionService;
            _localStorage = localStorage;
            _navigationService = navigationService;
            _chatService = chatService;
            _overlayService = overlayService;
            _messageRepository = new MessageRepository();
            _chatRepository = new ChatRepository();
            _message = new ObservableCollection<MessageViewModel>();
            PreviewPhoto = false;
            PreviewFile = false;
            ReplyMessage= false;
            LoadingSpinner = false;
            _serverInteractionService.MessageObservable.Subscribe(ProcessMessage);
        }

        public override void SetContext(object context)
        {
            _chatModel = (BaseChatModel)context;
            Username = $" Вы вошли как: {_localStorage.Username}";
            _chatRepository.ReadAllNonMyMessages(_chatModel.ChatId);
            var messageList = _messageRepository.GetMessagesByChatId(_chatModel.ChatId);
            switch (_chatModel.ChatType)
            {
                case ChatTypes.PtpChat:
                    _chatService.ReadMessageInPtpChat(_chatModel.ChatId);
                    var ptpChatModel = (PtpChatModel)_chatModel;
                    NameChat = $"Чат с {ptpChatModel.Username} ";
                    foreach (var message in messageList)
                    {
                        AddViewMessage(message.MessageId, message.Message.Content, message.SenderId,
                            message.Message.DateOfReceiving, null, message.Status);
                    }
                    break;
                case ChatTypes.GroupChat:
                    var groupChatModel = (GroupChatModel)_chatModel;
                    _chatService.ReadMessageInGroupChat(groupChatModel);
                    NameChat = $"Групповой чат {groupChatModel.NameChat} ";
                    foreach (var message in messageList)
                    {
                        AddViewMessage(message.MessageId, message.Message.Content, message.SenderId,
                            message.Message.DateOfReceiving, message.Message.Sender.Username, message.Status);
                    }
                    break;
            }
        }

        private async void SendingMessage()
        {
            if (Content is null && _fileInMessage is null)
            {
                return;
            }
            switch (_chatModel.ChatType)
            {
                case ChatTypes.PtpChat:
                    var ptpChatModel = (PtpChatModel)_chatModel;
                    if (_chatModel.ChatId == Guid.Empty)
                    {
                        _chatModel.ChatId = await _chatService.CreatePtpChat(ptpChatModel);
                    }
                    if (!_chatRepository.PtpChatIsExists(_chatModel.ChatId))
                    {
                        await _chatService.SavePtpChatInDatabase(ptpChatModel);
                    }
                    if (_fileInMessage != null)
                    {
                        LoadingSpinner = true;
                        var mediaMessagePtpChatDto = _chatService.SaveMediaMessageInPtpChat
                            (_chatModel.ChatId, Content, Reply, _fileInMessage.MediaType);
                        var ptpMessageId = mediaMessagePtpChatDto.Id;
                        await _chatService.SaveSentMedia(_fileInMessage, mediaMessagePtpChatDto.FileId);
                        AddViewMessage(ptpMessageId, Content, _localStorage.Id, DateTime.UtcNow);
                        LoadingSpinner= false;
                        PreviewFile = false;
                        Content= null;
                        ReplyMessage = false;
                        await _chatService.SendMediaMessage(mediaMessagePtpChatDto, _fileInMessage);
                    }
                    else
                    {
                        var ptpMessageId = _chatService.SaveAndSendPlainMessageInPtpChat(_chatModel.ChatId, Content, Reply);
                        AddViewMessage(ptpMessageId, Content, _localStorage.Id, DateTime.UtcNow);
                    }                    
                    break;
                case ChatTypes.GroupChat:
                    if (_fileInMessage != null)
                    {
                        LoadingSpinner = true;
                        var mediaMessageGroupChatDto = _chatService.SaveMediaMessageInGroupChat
                            (_chatModel, Content, Reply, _fileInMessage.MediaType);
                        var groupMessageId = mediaMessageGroupChatDto.Id;
                        await _chatService.SaveSentMedia(_fileInMessage, mediaMessageGroupChatDto.FileId);
                        AddViewMessage(groupMessageId, Content, _localStorage.Id, DateTime.UtcNow);
                        LoadingSpinner = false;
                        PreviewFile = false;
                        Content = null;
                        ReplyMessage = false;
                        await _chatService.SendMediaMessage(mediaMessageGroupChatDto, _fileInMessage);
                    }
                    else
                    {
                        var groupMessageId = _chatService.SaveAndSendPlainMessageInGroupChat(_chatModel, Content, Reply);
                        AddViewMessage(groupMessageId, Content, _localStorage.Id, DateTime.UtcNow);                        
                    }                    
                    break;
            }
            Content = null;
            _fileInMessage = null;
            FilePath = null;
            Reply= null;
            PreviewPhoto = false;            
            ReplyMessage= false;
        }

        private void ProcessMessage(BaseProtocolDto message)
        {
            switch (message.MessageType)
            {
                case MessageTypes.PlainMassage:
                    if (_chatModel is null || message.Chat.Id != _chatModel.ChatId)
                    {
                        return;
                    }
                    _messageRepository.MessageRead(_chatModel.ChatId);
                    var plainMessage = (PlainMessageDto)message;
                    ViewMessageInChat(plainMessage, plainMessage.Content);
                    break;
                case MessageTypes.MediaMessage:
                    if (_chatModel is null || message.Chat.Id != _chatModel.ChatId)
                    {
                        return;
                    }
                    _messageRepository.MessageRead(_chatModel.ChatId);
                    var mediaMessage = (MediaMessageDto)message;
                    ViewMessageInChat(mediaMessage, mediaMessage.Content);
                    break;
                case MessageTypes.WasReceive:
                    var wasReceiveMessage = (WasReceiveMessageDto)message;
                    for (var i = Message.Count - 1; i >= 0; i--)
                    {
                        if (Message[i].Id == wasReceiveMessage.MessageId)
                        {
                            Message[i].Icon = "Check";
                            break;
                        }
                    }
                    break;
                case MessageTypes.MessagesRead:
                    var messagesRead = (MessagesReadDto)message;
                    if (_chatModel is null || messagesRead.Chat.Id != _chatModel.ChatId)
                    {
                        return;
                    }
                    var j = Message.Count - 1;
                    while (j >= 0 && Message[j].IsOwn)
                    {
                        Message[j].Icon = "CheckAll";
                        j--;
                    }
                    break;
                case MessageTypes.DeleteMessage:
                    var deleteMessage = (DeleteMessageDto)message;
                    for (var i = Message.Count - 1; i >= 0; i--)
                    {
                        if (Message[i].Id == deleteMessage.MessageId)
                        {
                            Message.RemoveAt(i);
                            break;
                        }
                    }
                    break;
            }
        }

        private void ViewMessageInChat(BaseProtocolDto message, string content)
        {
            switch (_chatModel.ChatType)
            {
                case ChatTypes.PtpChat:
                    AddViewMessage(message.Id, content, message.Sender, DateTime.UtcNow);
                    _serverInteractionService.SendMessagesRead(new MessagesReadDto
                    {
                        Sender = _localStorage.Id,
                        Chat = new PtpChatDto
                        {
                            Id = _chatModel.ChatId
                        }
                    });
                    break;
                case ChatTypes.GroupChat:
                    var groupChatModel = (GroupChatModel)_chatModel;
                    _serverInteractionService.SendMessagesRead(new MessagesReadDto
                    {
                        Sender = _localStorage.Id,
                        Chat = new GroupChatDto
                        {
                            Id = groupChatModel.ChatId,
                            UsersId = groupChatModel.UsersId
                        }
                    });
                    var senderUsername = (_chatRepository.GetSenderUserByGroupChatId(message.Chat.Id, message.Sender)).Username;
                    AddViewMessage(message.Id, content, message.Sender, DateTime.UtcNow, senderUsername);
                    break;
            }
        }

        private async void ProcessDeletedMessage()
        {
            dynamic taskCheckDelete;
            if (_chatModel.ChatType == ChatTypes.GroupChat)
            {
                taskCheckDelete = _overlayService.ShowDialog<DeletedMessageDialogViewModel>(SelectedMessage.IsOwn);
            }
            else
            {
                taskCheckDelete = _overlayService.ShowDialog<DeletedMessageDialogViewModel>();
            }
            Overlay = (DeletedMessageDialogViewModel)_overlayService.Dialog;
            var deletedMessageDto = new DeleteMessageDto
            {
                Sender = _localStorage.Id,
                MessageId = SelectedMessage.Id,
            };
            switch (_chatModel.ChatType)
            {
                case ChatTypes.PtpChat:
                    deletedMessageDto.Chat = new PtpChatDto
                    {
                        Id = _chatModel.ChatId
                    };
                    break;
                case ChatTypes.GroupChat:
                    var groupChat = (GroupChatModel)_chatModel;
                    deletedMessageDto.Chat = new GroupChatDto
                    {
                        Id = groupChat.ChatId,
                        UsersId = groupChat.UsersId
                    };
                    break;
            }
            var checkDelete = await taskCheckDelete;
            if (checkDelete == null)
            {
                _overlayService.CloseDialog();
                Overlay = null;
                return;
            }
            if (checkDelete == true)
            {
                deletedMessageDto.DeleteMessageType = DeleteMessageTypes.EveryoneHas;
            }
            else
            {
                deletedMessageDto.DeleteMessageType = DeleteMessageTypes.JustMe;
            }
            _serverInteractionService.SendDeleteMessage(deletedMessageDto);
            _messageRepository.DeleteMessage(SelectedMessage.Id);
            Message.Remove(SelectedMessage);
            _overlayService.CloseDialog();
            Overlay = null;
        }

        private void GoBack()
        {
            _chatModel = null;
            _navigationService.NavigateBack(true);
        }

        private void AddViewMessage(
            Guid messageId, string content,
            Guid senderId, DateTime dateTime, string senderName = null,
            MessageStatuses status = MessageStatuses.NotSent)
        {
            var model = new MessageModel
            {
                MessageId = messageId,
                Content = content,
                SenderId = senderId,
                SenderName = senderName,
                Status = status,
                Time = dateTime.ToShortTimeString(),
                File = _messageRepository.GetFileIdInMessage(messageId)
            };
            var reply = _messageRepository.ReplyMessage(messageId);
            if (reply != null)
            {
                model.ReplyMessage = (ReplyMessage)reply;
            }
            var view = new MessageViewModel(_localStorage, model, _navigationService);
            Message.Add(view);
        }

        private async void ProcessImage()
        {
            var path = await OpenFileDialog(new FileDialogFilter()
            {
                Name = "All supported graphics",
                Extensions = { "*.jpg;*.jpeg;*.png" }
            });
            if (path is null)
            {
                return;
            }
            _fileInMessage = new FileInMessage
            {
                Path = path,
                MediaType = MediaTypes.PhotoMessage
            };
            FilePath = path;
            PreviewPhoto = true;
        }

        private async void ProcessFile()
        {
            var path = await OpenFileDialog(new FileDialogFilter()
            {
                Name = "WinRAR",
                Extensions = { "*.zip;*.rar" }
            });
            if (path is null)
            {
                return;
            }       
            _fileInMessage = new FileInMessage
            {
                Path = path,
                MediaType = MediaTypes.FileMessage
            };
            PreviewFile = true;
        }

        private async Task<string> OpenFileDialog(FileDialogFilter filter)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a file";
            op.Filters.Add(filter);
            string[] result = await op.ShowAsync(new Window());
            if (result != null)
            {
                return string.Join(" ", result);
            }
            return null;
        }

        private void ProcessRemoveMedia()
        {
            _fileInMessage = null;
            FilePath = null;
            PreviewPhoto = false;
            PreviewFile = false;
        }

        private void ProcessReplyMessage()
        {
            var reply = new ReplyMessage
            {
                Id = SelectedMessage.Id,
                Content = SelectedMessage.Content
            };
            if (SelectedMessage.IsOwn)
            {
                reply.SenderName = _localStorage.Username;
            }
            else
            {
                switch (_chatModel.ChatType)
                {
                    case ChatTypes.PtpChat:
                        var ptpChat = (PtpChatModel)_chatModel;
                        reply.SenderName = ptpChat.Username;
                        break;
                    case ChatTypes.GroupChat:
                        reply.SenderName = SelectedMessage.SenderName; 
                        break;
                }
            }
            if (SelectedMessage.PhotoMessage)
            {
                reply.PhotoMessage = true;
            }
            if (SelectedMessage.FileMessage)
            {
                reply.FileMessage= true;
            }
            Reply = reply;
            ReplyMessage = true;
        }
    }
}