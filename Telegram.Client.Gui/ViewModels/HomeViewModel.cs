﻿using ReactiveUI;
using System.Net.Http.Headers;
using System.Net.Http;
using System;
using System.Windows.Input;
using Telegram.Client.Gui.Services;
using Telegram.Core.Services.Models;
using System.Text.Json;
using Telegram.Client.Gui.Models;
using Telegram.Client.Core;

namespace Telegram.Client.Gui.ViewModels
{
    public class HomeViewModel : BasePageViewModel
    {
        public ICommand LoginCommand => ReactiveCommand.Create(ProcessLogin);

        private readonly INavigationService _navigationService;
        private readonly LocalStorage _localStorage;
        private readonly ServerInteractionService _serverInteractionService;
        private readonly RequestService _requestService;

        public HomeViewModel(INavigationService navigationService, LocalStorage localStorage, ServerInteractionService serverInteractionService)
        {
            _navigationService = navigationService;
            _localStorage = localStorage;
            _serverInteractionService = serverInteractionService;
            _requestService = new RequestService();
        }

        private void ProcessLogin()
        {
            _navigationService.NavigateTo<LoginViewModel>();
        }

        public override async void SetContext(object context)
        {
            var token = (string)context;

            var request = new HttpRequestMessage()
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"http://{Constants.Ip}/account/token?token={token}"),
            };
            var httpClient = new HttpClient();
            if (token != null)
            {
                httpClient.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", token);
            }
            var response = await httpClient.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var model = JsonSerializer.Deserialize<LoginResponseModel>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new LoginResponseModel();
                
                _localStorage.Id = model.Id;
                _localStorage.Token = model.Token;
                _localStorage.Username = model.Username;

                var connectService = new ConnectionService(_serverInteractionService);
                var connect = await connectService.ConnectToServer(_localStorage.Id, _localStorage.Token);
                if (connect)
                {
                    _navigationService.NavigateTo<ChatSelectionViewModel>();
                }
            }
        }
    }
}
