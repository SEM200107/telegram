using System;
using Telegram.Client.Database;
using Telegram.Client.Gui.Services;

namespace Telegram.Client.Gui.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public IObservable<ViewModelBase> Page => _navigationPageSource.Page;

        private readonly UserRepository _userRepository = new UserRepository();
        private readonly INavigationPageSource _navigationPageSource;

        public MainWindowViewModel(INavigationPageSource navigationPageSource, INavigationService navigationService)
        {
            _navigationPageSource = navigationPageSource;
            navigationService.NavigateTo<HomeViewModel>(_userRepository.GetToken());
        }
    }
}