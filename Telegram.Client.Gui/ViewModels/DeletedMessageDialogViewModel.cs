﻿using DynamicData.Kernel;
using ReactiveUI;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Telegram.Client.Gui.ViewModels
{
    public class DeletedMessageDialogViewModel : BasePageViewModel
    {
        public ICommand CancelCommand => ReactiveCommand.Create(ProcessCancel);
        public ICommand DeleteCommand => ReactiveCommand.Create(ProcessDelete);
        public bool CheckDeleteEveryone { get; set; } = false;
        public bool IsOwn
        {
            get => _isOwn;
            set => this.RaiseAndSetIfChanged(ref _isOwn, value);
        }
        public TaskCompletionSource<bool?> Tcs;

        private bool _isOwn = true;
        public DeletedMessageDialogViewModel()
        {
            Tcs = new TaskCompletionSource<bool?>();
        }

        public override void SetContext(object context)
        {
            IsOwn = (bool)context;
        }

        public override async Task<bool?> DialogResult()
        {
            return await Tcs.Task;
        }

        private void ProcessDelete()
        {
            Tcs.SetResult(CheckDeleteEveryone);
        }

        private void ProcessCancel()
        {
            Tcs.SetResult(null);
        }
    }
}
