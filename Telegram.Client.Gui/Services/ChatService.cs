﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Json;
using System.Security.Policy;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web.Mvc;
using Telegram.Client.Core;
using Telegram.Client.Core.Models;
using Telegram.Client.Database;
using Telegram.Client.Db.Context.Entity;
using Telegram.Client.Gui.Models;
using Telegram.Client.Gui.Models.Chats;
using Telegram.Core.Services.Models;
using Telegram.Core.Services.Models.Enums;
using Telegram.Core.Services.Models.Messages;
using Telegram.Core.Services.Models.Requests;

namespace Telegram.Client.Gui.Services
{
    public class ChatService
    {
        private readonly ServerInteractionService _serverInteractionService;
        private readonly LocalStorage _localStorage;
        private readonly RequestService _requestService;
        private readonly ChatRepository _chatRepository;
        private readonly MessageRepository _messageRepository;

        public ChatService(ServerInteractionService serverInteractionService, LocalStorage localStorage)
        {
            _serverInteractionService = serverInteractionService;
            _localStorage = localStorage;
            _requestService = new RequestService();
            _chatRepository = new ChatRepository();
            _messageRepository = new MessageRepository();
        }
        
        public void ReadMessageInPtpChat(Guid chatId)
        {
            if (chatId != Guid.Empty)
            {
                _serverInteractionService.SendMessagesRead(new MessagesReadDto
                {
                    Sender = _localStorage.Id,
                    Chat = new PtpChatDto
                    {
                        Id = chatId
                    }
                });
            }
        }

        public void ReadMessageInGroupChat(GroupChatModel model)
        {
            if (model.ChatId != Guid.Empty)
            {
                _serverInteractionService.SendMessagesRead(new MessagesReadDto
                {
                    Sender = _localStorage.Id,
                    Chat = new GroupChatDto
                    {
                        Id = model.ChatId,
                        UsersId = model.UsersId
                    }
                });
            }
        }

        public async Task<Guid> CreatePtpChat(PtpChatModel model)
        {
            var idList = new List<Guid>();
            idList.Add(_localStorage.Id);
            idList.Add(model.UserId);
            var creatingChat = new CreatingChatDto
            {
                ChatTypes = ChatTypes.PtpChat,
                UsersId = idList
            };
            var content = await _requestService.SendPostRequest(creatingChat,
                $"http://{Constants.Ip}/chat/creatingchat");
            return JsonSerializer.Deserialize<Guid>(content);
        }

        public async Task SavePtpChatInDatabase(PtpChatModel model)
        {
            var requestUser = await _requestService.SendGetRequest
                ($"http://{Constants.Ip}/user/getuser?userId={model.UserId}");
            var user = JsonSerializer.Deserialize<UserModel>
                (requestUser, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new UserModel();
            await _chatRepository.SaveChat(new PtpChat
            {
                Id = model.ChatId,
                FirstUserId = _localStorage.Id,
                SecondUser = new User
                {
                    Id = user.Id,
                    Username = user.Username,
                    Login = user.Login
                }
            });
        }

        public MediaMessageDto SaveMediaMessageInPtpChat(Guid chatId, string content, ReplyMessage reply, MediaTypes mediaType)
        {
            var fileId = Guid.NewGuid();            
            var mediaPtpMessage = new MediaMessageDto
            {
                Sender = _localStorage.Id,
                Chat = new PtpChatDto
                {
                    Id = chatId
                },
                Content = content,
                FileId = fileId,
                MediaType = mediaType
            };
            if (reply != null)
            {
                mediaPtpMessage.Reply = reply.Id;
            }
            var ptpMessageId = _messageRepository.SaveMediaMessage(mediaPtpMessage);
            mediaPtpMessage.Id = ptpMessageId;               
            return mediaPtpMessage;
        }

        public async Task SendMediaMessage(MediaMessageDto mediaMessageDto, FileInMessage file)
        {
            await SaveAndSendFile(file, mediaMessageDto.FileId);
            _serverInteractionService.SendMediaMessage(mediaMessageDto);            
        }

        public MediaMessageDto SaveMediaMessageInGroupChat(BaseChatModel model, string content, ReplyMessage reply, MediaTypes mediaType)
        {
            var groupChatModel = (GroupChatModel)model;
            var fileId = Guid.NewGuid();
            var mediaGroupMessage = new MediaMessageDto
            {
                Sender = _localStorage.Id,
                Chat = new GroupChatDto
                {
                    Id = groupChatModel.ChatId,
                    UsersId = groupChatModel.UsersId
                },
                Content = content,
                FileId = fileId,
                MediaType = mediaType
            };
            if (reply != null)
            {
                mediaGroupMessage.Reply = reply.Id;
            }
            var groupMessageId = _messageRepository.SaveMediaMessage(mediaGroupMessage);
            mediaGroupMessage.Id = groupMessageId;            
            return mediaGroupMessage;
        }

        public Guid SaveAndSendPlainMessageInPtpChat(Guid chatId, string content, ReplyMessage reply)
        {
            var plainPtpMessage = new PlainMessageDto
            {
                Sender = _localStorage.Id,
                Chat = new PtpChatDto
                {
                    Id = chatId
                },
                Content = content
            };
            if (reply != null)
            {
                plainPtpMessage.Reply = reply.Id;
            }
            var ptpMessageId = _messageRepository.SavePlainMessage(plainPtpMessage);
            plainPtpMessage.Id = ptpMessageId;
            _serverInteractionService.SendPlainMessage(plainPtpMessage);
            return ptpMessageId;
        }       

        public Guid SaveAndSendPlainMessageInGroupChat(BaseChatModel model, string content, ReplyMessage reply)
        {
            var groupChatModel = (GroupChatModel)model;
            var plainGroupMessage = new PlainMessageDto
            {
                Sender = _localStorage.Id,
                Chat = new GroupChatDto
                {
                    Id = groupChatModel.ChatId,
                    UsersId = groupChatModel.UsersId
                },
                Content = content
            };
            if (reply != null)
            {
                plainGroupMessage.Reply = reply.Id;
            }
            var groupMessageId = _messageRepository.SavePlainMessage(plainGroupMessage);
            plainGroupMessage.Id = groupMessageId;
            _serverInteractionService.SendPlainMessage(plainGroupMessage);
            return groupMessageId;
        }

        public async Task<PtpChatModel[]> SearchUserPtpChat(string searchUserName)
        {
            var chat = new SearchChatDto
            {
                SearchString = searchUserName,
                UserId = _localStorage.Id
            };
            var content = await _requestService.SendPostRequest(chat, $"http://{Constants.Ip}/chat/searchchat");
            return JsonSerializer.Deserialize<PtpChatModel[]>
                (content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? Array.Empty<PtpChatModel>();
        }        

        public async Task SaveSentMedia(FileInMessage file, Guid fileId)
        {
            if (!Directory.Exists("Download"))
            {
                Directory.CreateDirectory("Download");
            }
            var stream = File.OpenRead(file.Path);
            switch (file.MediaType)
            {
                case MediaTypes.PhotoMessage:
                    await SaveFile(stream, $"Download\\Photos\\{fileId}.png");
                    break;
                case MediaTypes.FileMessage:
                    await SaveFile(stream, $"Download\\Files\\{fileId}.rar");
                    break;
            }            
        }

        private async Task SaveAndSendFile(FileInMessage file, Guid fileId)
        {
            var httpClient = new HttpClient();
            httpClient.Timeout = TimeSpan.FromMinutes(10);
            var stream = File.OpenRead(file.Path);
            await httpClient.PostAsync($"http://{Constants.Ip}/chat/sendmedia?fileId={fileId}&mediaType={file.MediaType}", new StreamContent(stream));
        }

        private async Task SaveFile(Stream file, string path)
        {
            await using var fileStreamPhoto = File.OpenWrite(path);
            await file.CopyToAsync(fileStreamPhoto);
        }
    }
}
