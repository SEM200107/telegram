﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using System;
using Telegram.Client.Core;
using Telegram.Client.Gui.Models;
using Telegram.Client.Gui.ViewModels;
using Telegram.Core.Services.Services;

namespace Telegram.Client.Gui.Services
{
    public class ServiceCollectionBilder
    {
        public IServiceProvider Build()
        {
            var services = new ServiceCollection()
                .AddSingleton<NavigationService>()
                .AddSingleton<INavigationService>(provider => provider.GetRequiredService<NavigationService>())
                .AddSingleton<INavigationPageSource>(provider => provider.GetRequiredService<NavigationService>())
                .AddSingleton<OverlayService>()
                .AddSingleton<ServerInteractionService>()
                .AddSingleton<LocalStorage>()
                .AddSingleton<ChatService>()
                .AddSingleton(provider => new LoggerProvider(provider))
                .AddTransient<MainWindowViewModel>()
                .AddTransient<HomeViewModel>()
                .AddTransient<LoginViewModel>()
                .AddTransient<RegistrationViewModel>()
                .AddTransient<ChatViewModel>()
                .AddTransient<ChatSelectionViewModel>()
                .AddTransient<CreatingGroupChatViewModel>()
                .AddTransient<DeletedMessageDialogViewModel>()
                .AddTransient<ImageFullSizeViewModel>()
                .AddLogging(loggingBuilder =>
                {
                    loggingBuilder.ClearProviders();
                    loggingBuilder.SetMinimumLevel(LogLevel.Trace);
                    loggingBuilder.AddNLog();
                });

            return services.BuildServiceProvider();
        }
    }
}
