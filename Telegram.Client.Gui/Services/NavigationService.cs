﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reactive.Subjects;
using Telegram.Client.Gui.ViewModels;

namespace Telegram.Client.Gui.Services
{
    public class NavigationService : INavigationService, INavigationPageSource
    {
        public IObservable<BasePageViewModel> Page => _pageSubject;

        private BehaviorSubject<BasePageViewModel> _pageSubject;
        private Stack<BasePageViewModel> _pagesStack = new();
        private readonly IServiceProvider _serviceProvider;

        public NavigationService(IServiceProvider serviceProvider)
        {
            _pageSubject = new BehaviorSubject<BasePageViewModel>(null);
            _serviceProvider = serviceProvider;
        }

        public void NavigateTo<T>(object context = null)
            where T : BasePageViewModel
        {
            var model = _serviceProvider.GetRequiredService<T>();
            if (context != null)
            {
                model.SetContext(context);
            }

            _pagesStack.Push(model);
            _pageSubject.OnNext(model);
        }

        public void NavigateBack(bool reloadPageFlag = false)
        {
            _pagesStack.Pop();
            var model = _pagesStack.Peek();
            if (reloadPageFlag)
            {
                model.ReloadPage();
            }

            _pageSubject.OnNext(model);
        }
    }

    public interface INavigationPageSource
    {
        IObservable<BasePageViewModel> Page { get; }
    }

    public interface INavigationService
    {
        void NavigateTo<T>(object context = null)
            where T : BasePageViewModel;

        void NavigateBack(bool reloadPageFlag = false);
    }
}