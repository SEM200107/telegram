﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Client.Core;
using Telegram.Core.Services.Models;

namespace Telegram.Client.Gui.Services
{
    public class ConnectionService
    {
        private readonly ServerInteractionService _serverInteractionService;

        public ConnectionService(ServerInteractionService serverInteractionService)
        {
            _serverInteractionService = serverInteractionService;
        }

        public async Task<bool> ConnectToServer(Guid userId, string token)
        {
            var cancellationSource = new CancellationTokenSource();
            var cancellationToken = cancellationSource.Token;
            return await _serverInteractionService.Connect($"ws://{Constants.Ip}/connection/connect?userId={userId}", cancellationToken, token, userId);
        }
    }
}
