﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;
using Telegram.Client.Gui.ViewModels;

namespace Telegram.Client.Gui.Services
{
    public class OverlayService
    {
        public BasePageViewModel Dialog { get; set; }

        private readonly IServiceProvider _serviceProvider;

        public OverlayService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<bool?> ShowDialog<T>(object context = null)
            where T : BasePageViewModel
        {
            var model = _serviceProvider.GetRequiredService<T>();
            if (context != null)
            {
                model.SetContext(context);
            }
            Dialog = model;
            var tcs = model.DialogResult();
            return await tcs;
        }

        public void CloseDialog()
        {
            Dialog = null;
        }
    }
}
