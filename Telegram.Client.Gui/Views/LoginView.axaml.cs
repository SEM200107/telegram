using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace Telegram.Client.Gui.Views
{
    public partial class LoginView : UserControl
    {
        public LoginView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
