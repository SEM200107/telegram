using Avalonia;
using Avalonia.ReactiveUI;
using Microsoft.EntityFrameworkCore;
using System;
using Telegram.Client.Db.Context;

namespace Telegram.Client.Gui
{
    internal class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            using var _db = new DatabaseContext();
            _db.Database.MigrateAsync();

            BuildAvaloniaApp()
                .StartWithClassicDesktopLifetime(args);
        }

        public static AppBuilder BuildAvaloniaApp()
            => AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .LogToTrace()
                .UseReactiveUI();
    }
}
