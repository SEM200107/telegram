﻿using System;
using Telegram.Core.Services.Models.Enums;

namespace Telegram.Client.Gui.Models.Chats
{
    public class PtpChatModel : BaseChatModel
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string Login { get; set; }
        public override ChatTypes ChatType => ChatTypes.PtpChat;
    }
}
