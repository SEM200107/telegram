﻿using System;
using Telegram.Core.Services.Models.Enums;

namespace Telegram.Client.Gui.Models.Chats
{
    public abstract class BaseChatModel
    {
        public Guid ChatId { get; set; }
        public int CountNewMessage { get; set; }
        public bool NewMessage { get; set; } = false;
        public abstract ChatTypes ChatType { get; }
    }
}
