﻿using System;
using System.Collections.Generic;
using Telegram.Core.Services.Models.Enums;

namespace Telegram.Client.Gui.Models.Chats
{
    public class GroupChatModel : BaseChatModel
    {
        public string NameChat { get; set; }
        public List<Guid> UsersId { get; set; }
        
        public override ChatTypes ChatType => ChatTypes.GroupChat;
    }
}
