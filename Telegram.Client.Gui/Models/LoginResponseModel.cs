﻿using System;
using System.Text.Json.Serialization;

namespace Telegram.Client.Gui.Models
{
    public class LoginResponseModel
    {
        [JsonPropertyName("access_token")]
        public string Token { get; set; }
        public string Username { get; set; }
        [JsonPropertyName("userId")]
        public Guid Id { get; set; }
        public string Login { get; set; }
        [JsonPropertyName("errorText")]
        public string? Error { get; set; }
    }
}
