﻿using System;
using System.IO;
using Telegram.Core.Services.Models.Enums;

namespace Telegram.Client.Gui.Models
{
    public class FileInMessage
    {
        public string Path { get; set; }
        public MediaTypes MediaType { get; set; }
    }
}
