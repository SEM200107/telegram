﻿using System;
using Telegram.Client.Database.Models;
using Telegram.Client.Db.Context.Entity;

namespace Telegram.Client.Gui.Models
{
    public class MessageModel
    {
        public Guid MessageId { get; set; }
        public string Content { get; set; }
        public Guid SenderId { get; set; }
        public string SenderName { get; set; }
        public string Time { get; set; }
        public FileIdInMessage File { get; set; }
        public MessageStatuses Status { get; set; }
        public ReplyMessage ReplyMessage { get; set; }
    }

    public class ReplyMessage
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
        public string SenderName { get; set; }
        public bool PhotoMessage { get; set; } = false;
        public bool FileMessage { get; set; } = false;

        public static explicit operator ReplyMessage(Database.Models.ReplyMessage v)
        {
            return new ReplyMessage
            {
                Id = v.Id,
                Content = v.Content,
                FileMessage = v.FileMessage,
                PhotoMessage = v.PhotoMessage,
                SenderName = v.SenderName
            };
        }
    }
}
