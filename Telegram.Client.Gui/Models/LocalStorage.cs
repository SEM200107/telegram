﻿using System;
using System.Collections.Generic;

namespace Telegram.Client.Gui.Models
{
    public class LocalStorage
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
        public List<Guid> Chats { get; set; } = new List<Guid>();
    }
}
