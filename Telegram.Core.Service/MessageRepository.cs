﻿using Microsoft.EntityFrameworkCore;
using Telegram.Core.Db.Context.Entity;
using Telegram.Core.Services.Models.Enums;
using Telegram.Core.Services.Models.Messages;

namespace Telegram.Core.Server.Database
{
    public class MessageRepository : BaseRepository
    {
        private readonly ChatRepository _chatRepository = new ChatRepository();
        public async Task SavePlainMessage(PlainMessageDto model)
        {
            using var database = GetDb();
            var message = new Message
            {
                Id = model.Id,
                SenderId = model.Sender,
                ChatId = model.Chat.Id,
                Content = model.Content,
                Reply = model.Reply,
            };
            await database.Messages.AddAsync(message);            
            await database.SaveChangesAsync();
            SaveWasReceived(model.Chat, message);
        }

        public async Task SaveMediaMessage(MediaMessageDto model)
        {
            using var database = GetDb();
            var message = new Message
            {
                Id = model.Id,
                SenderId = model.Sender,
                ChatId = model.Chat.Id,
                Content = model.Content,
                FileId = Guid.NewGuid(),
                Reply= model.Reply,
                MediaType= model.MediaType,
            };
            await database.Messages.AddAsync(message);
            await database.SaveChangesAsync();
            SaveWasReceived(model.Chat, message);
        }

        public void MessageIsReceived(Guid messageId, Guid recipientId)
        {
            using var database = GetDb();
            var wasReceived = database.WasReceiveds
                .FirstOrDefault(x => x.MessageId == messageId && x.RecipientId == recipientId);
            wasReceived.IsReceived = true;
            database.SaveChanges();
        }

        public List<Message> GetListUnsentMessage(Guid userId)
        {
            using var database = GetDb();
            var result = new List<Message>();
            var wasReceiveds = database.WasReceiveds
                .Include(x => x.Message)
                .Include(x => x.Message.Chat)
                .Where(x => !x.IsReceived && x.RecipientId == userId)
                .ToList();
            foreach (var wasReceived in wasReceiveds)
            {
                result.Add(wasReceived.Message);
            }
            return result;
        }

        public List<Message> SynchronizationMessage(List<Guid> messagesId, Guid userId)
        {
            using var database = GetDb();
            var result = new List<Message>();
            var messagesIdInDb = new List<Guid>();

            var wasReceivedsMessages = database.WasReceiveds
                .Where(x => x.RecipientId == userId && !x.IsDeleted) 
                .Include(x => x.Sender)
                .Include(x => x.Recipient)
                .Include(x => x.Message)
                .ToList();
            var messages = database.Messages 
                .Where(x => x.SenderId == userId && !x.IsDeleted)
                .Include(x => x.Sender)
                .ToList();
            if (messagesId.Count <= 0) 
            {
                foreach(var wasReceived in wasReceivedsMessages)
                {
                    result.Add(wasReceived.Message);
                }
                result.AddRange(messages);
                return result;
            }

            foreach (var wasReceived in wasReceivedsMessages)
            {
                messagesIdInDb.Add(wasReceived.MessageId);
            }
            foreach (var message in messages)
            {
                messagesIdInDb.Add(message.Id);
            }
            var exceptId = messagesIdInDb.Except(messagesId).ToList();

            if (exceptId.Count == 0)
            {
                return result;
            }

            foreach (var id in exceptId)
            {
                result.Add(database.Messages.Find(id));
            }
            return result;
        }

        public void DeletingMessageFromOne (Guid messageId, Guid senderId)
        {
            using var database = GetDb();
            var messageIsOwn = database.Messages
                .FirstOrDefault(x => x.Id == messageId && x.SenderId == senderId);
            if (messageIsOwn != null)
            {
                messageIsOwn.IsDeleted = true;
            }
            else
            {
                var messageNotIsOwn = database.WasReceiveds
                    .FirstOrDefault(x => x.MessageId == messageId && x.RecipientId == senderId);
                messageNotIsOwn.IsDeleted = true;
            }            
            database.SaveChanges();
        }

        public void DeletingMessageForEveryone(Guid messageId)
        {
            using var database = GetDb();
            var message = database.Messages.Find(messageId);
            database.Messages.Remove(message);
            database.SaveChanges();
        }

        private void SaveWasReceived (BaseChatDto chat, Message message)
        {
            using var database = GetDb();
            switch (chat.ChatTypes)
            {
                case ChatTypes.PtpChat:
                    var recipientId = _chatRepository.GetSecondUserIdInPtpChat(chat.Id, message.SenderId);
                    var wasReceivedPtp = new WasReceived
                    {
                        SenderId = message.SenderId,
                        RecipientId = recipientId,
                        MessageId = message.Id
                    };
                    database.WasReceiveds.Add(wasReceivedPtp);
                    break;
                case ChatTypes.GroupChat:
                    var usersIdList = ((GroupChatDto)chat).UsersId;
                    foreach (var userId in usersIdList)
                    {
                        if (userId == message.SenderId)
                        {
                            continue;
                        }
                        var wasReceivedGroup = new WasReceived
                        {
                            SenderId = message.SenderId,
                            RecipientId = userId,
                            MessageId = message.Id
                        };
                        database.WasReceiveds.Add(wasReceivedGroup);
                    }
                    break;
            }
            database.SaveChanges();
        }
    }
}
