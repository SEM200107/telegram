﻿using Microsoft.EntityFrameworkCore;
using System.Text;
using Telegram.Core.Db.Context.Entity;
using Telegram.Core.Services.Models.Requests;

namespace Telegram.Core.Server.Database
{
    public class UserRepository : BaseRepository
    {
        public async Task<bool> RegistrationUser(RegistrationDto model)
        {
            using var database = GetDb();
            var userInDb = database.Users.FirstOrDefault(x => x.Login == model.Login);
            if (userInDb != null)
            {
                return false;
            }
            var password = Encoding.UTF8.GetString(model.Password);
            var user = new User
            {
                Username = model.Username,
                Login = model.Login,
                Password = HashPass.HashPassword(password)
            };            
            await database.Users.AddAsync(user);
            await database.SaveChangesAsync();
            return true;
        }

        public User GetUserById(Guid id)
        {
            using var database = GetDb();
            return database.Users.Find(id);
        }

        public async Task<User> LoginUser(LoginDto model)
        {
            using var database = GetDb();
            var user = await database.Users.FirstOrDefaultAsync(x => x.Login == model.Login);
            var password = Encoding.UTF8.GetString(model.Password);
            if (user == null || !HashPass.VerifyHashedPassword(user.Password, password))
            {
                return null;
            }
            return user;
        }

        public List<User> GetListUserByUsername(SearchChatDto model)
        {
            using var database = GetDb();
            return database.Users.Where(x => x.Id != model.UserId && (x.Username == model.SearchString || x.Login == model.SearchString)).ToList();
        }

        public List<User> GetListUsersByPtpChatsId(GetChatsDto model)
        {
            using var database = GetDb();
            var users = new List<User>();
            foreach (var chatId in model.ChatsList)
            {
                var ptpChat = database.PtpChats
                    .Include(x => x.SecondUser)
                    .Include(x => x.FirstUser)
                    .FirstOrDefault(x => x.Id == chatId);
                if (ptpChat == null)
                {
                    continue;
                }
                if (ptpChat.FirstUserId == model.Sender)
                {
                    users.Add(ptpChat.SecondUser);
                }
                else
                {
                    users.Add(ptpChat.FirstUser);
                }
            }
            return users;
        }
    }
}
