﻿using Telegram.Core.Db.Context;

namespace Telegram.Core.Server.Database
{
    public class BaseRepository
    {
        public DatabaseContext GetDb()
        {
            return new DatabaseContext(DbConfiguration.Configuration["ConnectionString"]);
        }
    }
}