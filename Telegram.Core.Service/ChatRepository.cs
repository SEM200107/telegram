﻿using System.Data.Entity;
using Telegram.Core.Db.Context.Entity;
using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Server.Database
{
    public class ChatRepository : BaseRepository
    {
        public Guid SaveChat(BaseChat chat)
        {
            using var database = GetDb();
            switch (chat.ChatTypes)
            {
                case ChatTypes.PtpChat:
                    database.PtpChats.Add((PtpChat)chat);
                    break;
                case ChatTypes.GroupChat:
                    database.GroupChats.Add((GroupChat)chat);
                    break;
            }
            database.SaveChanges();
            return chat.Id;
        }

        public GroupChat GetGroupChatById(Guid id)
        {
            using var database = GetDb();
            var chat = database.GroupChats
                .Include(x => x.Users)
                .FirstOrDefault(x => x.Id == id);
            return chat;
        }

        public Guid GetSecondUserIdInPtpChat(Guid chatId, Guid firstUserId)
        {
            using var database = GetDb();
            var chat = database.PtpChats.Find(chatId);
            if (chat.FirstUserId == firstUserId)
            {
                return chat.SecondUserId;
            }
            else
            {
                return chat.FirstUserId;
            }
        }

        public PtpChat GetPtpChatByIdUsers(Guid firstUserId, Guid secondUserId)
        {
            using var database = GetDb();
            return database.PtpChats.FirstOrDefault(x =>
                (x.FirstUserId == firstUserId && x.SecondUserId == secondUserId) ||
                (x.FirstUserId == secondUserId && x.SecondUserId == firstUserId));
        }

        public List<PtpChat> SynchronizationPtpChat(List<Guid> chatsId, Guid userId)
        {
            using var database = GetDb();
            var result = new List<PtpChat>();
            var chatsIdInDb = new List<Guid>();

            var chats = database.PtpChats
                .Where(x => x.FirstUserId == userId || x.SecondUserId == userId)
                .ToList();
            foreach (var chat in chats) // вместо Include, т.к. он не работает
            {
                var firstUser = database.Users.Find(chat.FirstUserId);
                var secondUser = database.Users.Find(chat.SecondUserId);
                chat.FirstUser = firstUser;
                chat.SecondUser = secondUser;
                chatsIdInDb.Add(chat.Id);
            }

            if (chatsId.Count <= 0)
            {
                result.AddRange(chats);
                return result;
            }

            var exceptId = chatsIdInDb.Except(chatsId).ToList();

            if (exceptId.Count == 0)
            {
                return result;
            }

            foreach(var id in exceptId)
            {
                result.Add(database.PtpChats.Find(id));
            }
            return result;
        }

        public List<GroupChat> SynchronizationGroupChat(List<Guid> chatsId, Guid userId)
        {
            using var database = GetDb();
            var result = new List<GroupChat>();
            var chatsIdInDb = new List<Guid>();
            var user = database.Users.Find(userId);
            var chats = new List<GroupChat>();

            var allChats = database.GroupChats
                .Include(x => x.Users)
                .ToList();
            foreach(var chat in allChats)
            {
                foreach (var userInChat in chat.Users)
                {
                    if (user.Id == userInChat.Id)
                    {
                        chats.Add(chat);
                    }
                }                
            }

            if (chatsId.Count <= 0)
            {
                result.AddRange(chats);
                return result;
            }

            foreach (var chat in chats)
            {
                chatsIdInDb.Add(chat.Id);
            }
            var exceptId = chatsIdInDb.Except(chatsId).ToList();

            if (exceptId.Count == 0)
            {
                return result;
            }

            foreach (var id in exceptId)
            {
                result.Add(database.GroupChats.Find(id));
            }
            return result;
        }
    }
}
