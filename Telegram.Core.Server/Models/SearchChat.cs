﻿namespace Telegram.Core.Server.Models
{
    public class SearchChat
    {
        public Guid UserId { get; set; }
        public Guid ChatId { get; set; }
        public string Username { get; set; }
        public string Login { get; set; }
    }
}
