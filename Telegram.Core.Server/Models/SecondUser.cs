﻿namespace Telegram.Core.Server.Models
{
    public class SecondUser
    {
        public Guid ChatId { get; set; }
        public Guid UserId { get; set; }
    }
}
