﻿using System.IO;
using Telegram.Core.Db.Context.Entity;
using Telegram.Core.Server.Database;
using Telegram.Core.Services.Models;
using Telegram.Core.Services.Models.Enums;
using Telegram.Core.Services.Models.Messages;

namespace Telegram.Core.Server.Services
{
    public class MessageProcessor
    {
        private readonly MessageRepository _messageRepository;
        private readonly ChatRepository _chatRepository;
        private readonly IMessageSender _messageSender;
        private IDisposable _messageSubscription;

        public MessageProcessor(IMessageSender messageSender)
        {
            _messageRepository = new MessageRepository();
            _chatRepository = new ChatRepository();
            _messageSender = messageSender;
        }

        public void SubscribeForMessages(IObservable<BaseProtocolDto> messageObservable)
        {
            _messageSubscription = messageObservable.Subscribe(ProcessMessage);
        }

        private async void ProcessMessage(BaseProtocolDto message)
        {
            switch (message.MessageType)
            {
                case MessageTypes.PlainMassage:
                    var plainMassage = (PlainMessageDto)message;
                    await _messageRepository.SavePlainMessage(plainMassage);
                    var receivedPlainMessage = new WasReceiveMessageDto
                    {
                        MessageId = plainMassage.Id,
                        Sender = Constants.ServerGuid
                    };
                    _messageSender.SendMessage(receivedPlainMessage, message.Sender);
                    switch (plainMassage.Chat.ChatTypes)
                    {
                        case ChatTypes.PtpChat:
                            var recipientPtp = _chatRepository.GetSecondUserIdInPtpChat(message.Chat.Id, message.Sender);                            
                            _messageSender.SendMessage(plainMassage, recipientPtp);
                            break;
                        case ChatTypes.GroupChat:
                            var gropChat = (GroupChatDto)plainMassage.Chat;
                            gropChat.UsersId.Remove(message.Sender);
                            foreach (var recipientGroup in gropChat.UsersId)
                            {
                                _messageSender.SendMessage(plainMassage, recipientGroup);
                            }
                            break;
                    }                    
                    break;
                case MessageTypes.WasReceive:
                    var wasReceiveMessage = (WasReceiveMessageDto)message;
                     _messageRepository.MessageIsReceived(wasReceiveMessage.MessageId, wasReceiveMessage.Sender);
                    break;
                case MessageTypes.MessagesRead:
                    var messagesReadDto = (MessagesReadDto)message;
                    var messagesReadModel = new MessagesReadDto
                    {
                        Sender = Constants.ServerGuid,
                        Chat = messagesReadDto.Chat,
                    };
                    switch (messagesReadDto.Chat.ChatTypes)
                    {
                        case ChatTypes.PtpChat:
                            var recipient = _chatRepository.GetSecondUserIdInPtpChat(messagesReadDto.Chat.Id, message.Sender);
                            _messageSender.SendMessage(messagesReadModel, recipient);
                            break;
                        case ChatTypes.GroupChat:
                            var groupChat = (GroupChatDto)messagesReadDto.Chat;
                            foreach (var userId in groupChat.UsersId)
                            {
                                if (userId != messagesReadDto.Sender)
                                {
                                    _messageSender.SendMessage(messagesReadModel, userId);
                                }                                
                            }
                            break;
                    }                    
                    break;
                case MessageTypes.DeleteMessage:
                    var deleteMessageDto = (DeleteMessageDto)message;                    
                    switch (deleteMessageDto.DeleteMessageType)
                    {
                        case DeleteMessageTypes.JustMe:
                            _messageRepository.DeletingMessageFromOne(deleteMessageDto.MessageId, deleteMessageDto.Sender);
                            break;
                        case DeleteMessageTypes.EveryoneHas:
                            var deleteMessageModel = new DeleteMessageDto
                            {
                                Sender = Constants.ServerGuid,
                                Chat = deleteMessageDto.Chat,
                                MessageId = deleteMessageDto.MessageId,
                                DeleteMessageType = deleteMessageDto.DeleteMessageType,
                            };
                            switch (deleteMessageDto.Chat.ChatTypes)
                            {
                                case ChatTypes.PtpChat:
                                    _messageRepository.DeletingMessageForEveryone(deleteMessageDto.MessageId);
                                    var recipient = _chatRepository.GetSecondUserIdInPtpChat(deleteMessageDto.Chat.Id, message.Sender);
                                    _messageSender.SendMessage(deleteMessageModel, recipient);
                                    break;
                                case ChatTypes.GroupChat:
                                    var groupChat = (GroupChatDto)deleteMessageDto.Chat;
                                    _messageRepository.DeletingMessageForEveryone(deleteMessageDto.MessageId);
                                    foreach (var userId in groupChat.UsersId)
                                    {
                                        if (userId != deleteMessageDto.Sender)
                                        {
                                            _messageSender.SendMessage(deleteMessageModel, userId);
                                        }
                                    }
                                    break;
                            }
                            break;
                    }
                    break;
                case MessageTypes.MediaMessage:
                    var mediaMessageDto = (MediaMessageDto)message;
                    await _messageRepository.SaveMediaMessage(mediaMessageDto);
                    var receivedMediaMessage = new WasReceiveMessageDto
                    {
                        MessageId = mediaMessageDto.Id,
                        Sender = Constants.ServerGuid
                    };
                    _messageSender.SendMessage(receivedMediaMessage, message.Sender);                    
                    switch (mediaMessageDto.Chat.ChatTypes)
                    {
                        case ChatTypes.PtpChat:
                            var recipientPtp = _chatRepository.GetSecondUserIdInPtpChat(message.Chat.Id, message.Sender);                            
                            _messageSender.SendMessage(mediaMessageDto, recipientPtp);
                            break;
                        case ChatTypes.GroupChat:
                            var gropChat = (GroupChatDto)mediaMessageDto.Chat;
                            gropChat.UsersId.Remove(message.Sender);
                            foreach (var recipientGroup in gropChat.UsersId)
                            {
                                _messageSender.SendMessage(mediaMessageDto, recipientGroup);
                            }
                            break;
                    }
                    break;
            }
        }

        public void UnsentMessages(Guid userId)
        {
            var messageList = _messageRepository.GetListUnsentMessage(userId);
            if (messageList.Count == 0)
            {
                return;
            }
            foreach (var message in messageList)
            {
                switch (message.Chat.ChatTypes)
                {
                    case ChatTypes.PtpChat:
                        var plainPtpMessage = new PlainMessageDto
                        {
                            Id = message.Id,
                            Content = message.Content,
                            Chat = new PtpChatDto
                            {
                                Id = message.ChatId
                            },
                            Sender = message.SenderId
                        };
                        _messageSender.SendMessage(plainPtpMessage, userId);
                        break;
                    case ChatTypes.GroupChat:
                        var usersId = new List<Guid>();
                        var groupChat = (GroupChat)message.Chat;
                        foreach (var user in groupChat.Users)
                        {
                            usersId.Add(user.Id);
                        }
                        var plainGroupMessage = new PlainMessageDto
                        {
                            Id = message.Id,
                            Content = message.Content,
                            Chat = new GroupChatDto
                            {
                                Id = message.ChatId,
                                UsersId = usersId
                            },
                            Sender = message.SenderId
                        };
                        _messageSender.SendMessage(plainGroupMessage, userId);
                        break;
                }
                
            }
        }        
    }
}