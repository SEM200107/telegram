﻿using System.Collections.Concurrent;
using System.Net.WebSockets;
using Telegram.Core.Server.Services;
using Telegram.Core.Services.Models.Messages;
using Telegram.Core.Services.Services;

namespace Telegram.Core.Server
{
    public class ActiveConnectionsService : IMessageSender
    {
        public IReadOnlyDictionary<Guid, ClientMessagesListener> ActiveConnections => _activeConnections;

        private MessageProcessor _messageProcessor;
        private readonly ConcurrentDictionary<Guid, ClientMessagesListener> _activeConnections;
        private readonly LoggerProvider _loggerProvider;

        public ActiveConnectionsService(LoggerProvider loggerProvider)
        {
            _activeConnections = new ConcurrentDictionary<Guid, ClientMessagesListener>();
            _loggerProvider = loggerProvider;
        }

        public async Task StoreConnection(Guid userId, WebSocket socket)
        {
            var listener = new ClientMessagesListener(socket, _loggerProvider.GetLogger<ClientMessagesListener>());
            var startTask = listener.StartListen();
            _activeConnections[userId] = listener;

            _messageProcessor = new MessageProcessor(this);            
            _messageProcessor.SubscribeForMessages(listener.MessageObservable);
            _messageProcessor.UnsentMessages(userId);

            await startTask;
        }

        public void StopConnection(Guid userId)
        {
            var lisener = _activeConnections[userId];
            lisener.Stop();
        }        

        public void SendMessage (BaseProtocolDto message, Guid recipient)
        {
            try
            {
                _activeConnections[recipient].EnqueueMessage(message);
            }
            catch (Exception)
            {
                return;
            }
        }
    }
}