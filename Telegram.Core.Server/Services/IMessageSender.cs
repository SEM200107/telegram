﻿using Telegram.Core.Services.Models.Messages;

namespace Telegram.Core.Server
{
    public interface IMessageSender
    {
        void SendMessage(BaseProtocolDto message, Guid recipient);
    }
}