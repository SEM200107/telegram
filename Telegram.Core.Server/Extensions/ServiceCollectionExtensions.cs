﻿using Telegram.Core.Services.Services;

namespace Telegram.Core.Server.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCore(this IServiceCollection services)
        {
            return services
                .AddSingleton<ActiveConnectionsService>()
                .AddTransient<MessageEncoderDecoder>()
                .AddSingleton<LoggerProvider>(provider => new LoggerProvider(provider));
                ;
        }
    }
}
