﻿using Microsoft.AspNetCore.Mvc;
using Telegram.Core.Server.Database;

namespace Telegram.Core.Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly UserRepository _userRepository = new UserRepository();

        [HttpGet("getuser")]
        public IActionResult GetUser([FromQuery] Guid userId)
        {
            var user = _userRepository.GetUserById(userId);
            var result = new
            {
                Id = user.Id,
                Username = user.Username,
                Login = user.Login
            };
            return Json(result);
        }
    }
}
