﻿using Microsoft.AspNetCore.Mvc;
using Telegram.Core.Db.Context.Entity;
using Telegram.Core.Server.Database;
using Telegram.Core.Server.Models;
using Telegram.Core.Services.Models.Enums;
using Telegram.Core.Services.Models.Messages;
using Telegram.Core.Services.Models.Requests;

namespace Telegram.Core.Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ChatController : Controller
    {
        private readonly UserRepository _userRepository = new UserRepository();
        private readonly ChatRepository _chatRepository = new ChatRepository();

        [HttpPost("searchchat")]
        public IActionResult SearchChat(SearchChatDto model)
        {
            var userList = _userRepository.GetListUserByUsername(model);
            var result = new List<SearchChat>();
            if (userList.Count > 0)
            {
                foreach (var user in userList)
                {
                    var chat = _chatRepository.GetPtpChatByIdUsers(model.UserId, user.Id);
                    var chatId = Guid.Empty;
                    if (chat != null)
                    {
                        chatId = chat.Id;
                    }
                    var searchChat = new SearchChat
                    {
                        ChatId = chatId,
                        Login = user.Login,
                        UserId = user.Id,
                        Username = user.Username
                    };
                    result.Add(searchChat);
                }
            }
            return Json(result);
        }

        [HttpPost("creatingchat")]
        public Guid CreatingChat(CreatingChatDto model)
        {
            switch (model.ChatTypes)
            {
                case ChatTypes.PtpChat:
                    var chat = _chatRepository.GetPtpChatByIdUsers(model.UsersId[0], model.UsersId[1]);
                    if (chat != null)
                    {
                        return chat.Id;
                    }
                    var newPtpChat = new PtpChat
                    {
                        FirstUserId = model.UsersId[0],
                        SecondUserId = model.UsersId[1],
                    };
                    _chatRepository.SaveChat(newPtpChat);
                    return newPtpChat.Id;
                case ChatTypes.GroupChat:
                    var userList = new List<User>();
                    foreach (var id in model.UsersId)
                    {
                        var user = _userRepository.GetUserById(id);
                        userList.Add(user);
                    }
                    var groupChat = new GroupChat
                    {
                        NameChat = model.NameChat,
                        Users = userList,
                    };
                    _chatRepository.SaveChat(groupChat);
                    return groupChat.Id;
            }
            return Guid.Empty;
        }

        [HttpGet("getgroupchatinformation")]
        public GroupChatInformation GetGroupChatInformation([FromQuery] Guid chatId)
        {
            var chat = _chatRepository.GetGroupChatById(chatId);
            var chatInforation = new GroupChatInformation
            {
                NameChat = chat.NameChat,
                Users = new List<UserInformation>()
            };
            foreach (var user in chat.Users)
            {
                var userInformation = new UserInformation
                {
                    Id = user.Id,
                    Name = user.Username
                };
                chatInforation.Users.Add(userInformation);
            }
            return chatInforation;
        }

        [HttpGet("downloadphoto")]
        public byte[] DownloadPhoto([FromQuery] Guid mediaId)
        {
            return System.IO.File.ReadAllBytes($"Media\\{mediaId}.png");
        }

        [HttpGet("downloadfile")]
        public IActionResult DownloadFile([FromQuery] Guid mediaId)
        {
            var stream = System.IO.File.OpenRead($"Media\\{mediaId}.rar");
            return new FileStreamResult(stream, "application/zip")
            {
                FileDownloadName = $"Media\\{mediaId}.rar"
            };
        }

        [HttpPost("sendmedia")]
        public async Task SaveMedia([FromQuery] Guid fileId, [FromQuery] MediaTypes mediaType)
        {
            if (!Directory.Exists("Media"))
            {
                Directory.CreateDirectory("Media");
            }
            var stream = Request.Body;
            switch (mediaType)
            {
                case MediaTypes.PhotoMessage:
                    await SaveFile(stream, $"Media\\{fileId}.png");
                    break;
                case MediaTypes.FileMessage:
                    await SaveFile(stream, $"Media\\{fileId}.rar");
                    break;
            }
        }

        private async Task SaveFile(Stream stream, string path)
        {
            await using var fileStream = System.IO.File.OpenWrite(path);
            await stream.CopyToAsync(fileStream);
        }
    }
}
