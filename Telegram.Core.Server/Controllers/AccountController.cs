﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Telegram.Core.Db.Context.Entity;
using Telegram.Core.Server.Auth;
using Telegram.Core.Server.Database;
using Telegram.Core.Services.Models.Requests;

namespace Telegram.Core.Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly UserRepository _userRepository = new UserRepository();

        [HttpPost("registration")]
        public async Task<bool> RegistrationUser([FromBody] RegistrationDto model)
        {
            return await _userRepository.RegistrationUser(model);
        }

        [Authorize]
        [HttpGet("token")]
        public IActionResult GetInfoByToken([FromQuery] string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(token);
            var tokenS = jsonToken as JwtSecurityToken;

            var userId = tokenS.Claims.First(claim => claim.Type == "userId").Value;
            var username = tokenS.Claims.First(claim => claim.Type == "username").Value;
            var response = new
            {
                userId = userId,
                access_token = token,
                username = username,
            };
            return Json(response);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginDto model)
        {
            var user = await _userRepository.LoginUser(model);
            if (user == null)
            {
                return BadRequest(new { errorText = "Неверный логин или пароль!" });
            }

            var identity = GetIdentity(user);
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                userId = user.Id,
                access_token = encodedJwt,
                username = user.Username,
                login = user.Login
            };
            return Json(response);
        }

        private ClaimsIdentity GetIdentity(User user)
        {
            var claims = new List<Claim>
                {
                    new Claim("username", user.Username),
                    new Claim("userId", user.Id.ToString())
                };
            ClaimsIdentity claimsIdentity =
            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}
