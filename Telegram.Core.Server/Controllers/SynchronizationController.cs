﻿using Microsoft.AspNetCore.Mvc;
using Telegram.Core.Server.Database;
using Telegram.Core.Services.Models.Requests;

namespace Telegram.Core.Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SynchronizationController : Controller
    {
        private readonly MessageRepository _messageRepository = new MessageRepository();
        private readonly ChatRepository _chatRepository = new ChatRepository();

        [HttpPost]
        public SynchronizationModelResponse Synchronization(SynchronizationModelRequest model)
        {
            var result = new SynchronizationModelResponse();
            var ptpChats = _chatRepository.SynchronizationPtpChat(model.PtpChatsId, model.UserId);
            var groupChats = _chatRepository.SynchronizationGroupChat(model.GroupChatsId, model.UserId);
            var messages = _messageRepository.SynchronizationMessage(model.MessagesId, model.UserId);

            if (ptpChats.Count > 0)
            {
                foreach (var chat in ptpChats)
                {
                    var user = chat.FirstUser;
                    if (chat.FirstUserId == model.UserId)
                    {
                        user = chat.SecondUser;
                    }
                    result.PtpChats.Add(new SynchronizationPtpChatModel
                    {
                        ChatId = chat.Id,
                        Login = user.Login,
                        UserId = user.Id,
                        Username = user.Username
                    });
                }
            }

            if (groupChats.Count > 0)
            {
                foreach (var chat in groupChats)
                {
                    var users = new List<UserInformation>();
                    foreach (var user in chat.Users)
                    {
                        if (user.Id == model.UserId)
                        {
                            continue;
                        }
                        users.Add(new UserInformation
                        {
                            Id = user.Id,
                            Name = user.Username,
                            Login = user.Login
                        });
                    }
                    result.GroupChats.Add(new GroupChatInformation
                    {
                        ChatId = chat.Id,
                        NameChat = chat.NameChat,
                        Users = users
                    });
                }
            }

            if (messages.Count > 0)
            {
                foreach (var message in messages)
                {
                    result.Messages.Add(new SynchronizationMessageModel
                    {
                        MessageId = message.Id,
                        ChatId = message.ChatId,
                        Content = message.Content,
                        DateOfReceiving = message.CreatedDate,
                        SenderId = message.SenderId,
                        FileId = message.FileId,
                        Reply = message.Reply,
                        MediaType = message.MediaType
                    });
                }
            }
            return result;
        }
    }
}
