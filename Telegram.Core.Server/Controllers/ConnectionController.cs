﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Telegram.Core.Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ConnectionController : Controller
    {
        private readonly ActiveConnectionsService _activeConnectionsService;

        public ConnectionController(ActiveConnectionsService activeConnectionsService)
        {
            _activeConnectionsService = activeConnectionsService;
        }

        [Authorize]
        [HttpGet("connect")]
        public async Task<IActionResult> OpenConnection(Guid userId)
        {
            if (!HttpContext.WebSockets.IsWebSocketRequest)
            {
                return BadRequest();
            }

            var socket = await HttpContext.WebSockets.AcceptWebSocketAsync();

            await _activeConnectionsService.StoreConnection(userId, socket);

            return Ok();
        }
    }
}
