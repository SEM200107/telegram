﻿using Telegram.Core.Services.Models.Messages;

namespace Telegram.Client.Core
{
    public interface IMessageSender
    {
        void SendPlainMessage(PlainMessageDto model);
        void SendWasReceiveMessage(WasReceiveMessageDto model);
    }
}
