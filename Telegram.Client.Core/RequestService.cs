﻿using System.Net.Http.Json;

namespace Telegram.Client.Core
{
    public class RequestService
    {
        public async Task<string> SendPostRequest(object model, string url)
        {
            var request = new HttpRequestMessage()
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(url),
                Content = JsonContent.Create(model)
            };
            var httpClient = new HttpClient();
            var response = await httpClient.SendAsync(request);
            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> SendGetRequest(string url)
        {
            var request = new HttpRequestMessage()
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(url),
            };
            var httpClient = new HttpClient();
            var response = await httpClient.SendAsync(request);
            return await response.Content.ReadAsStringAsync();
        }
    }
}
