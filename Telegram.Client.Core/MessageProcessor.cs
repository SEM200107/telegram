﻿using System.Text.Json;
using Telegram.Client.Core.Models;
using Telegram.Client.Database;
using Telegram.Client.Db.Context.Entity;
using Telegram.Core.Services.Models;
using Telegram.Core.Services.Models.Enums;
using Telegram.Core.Services.Models.Messages;
using Telegram.Core.Services.Models.Requests;

namespace Telegram.Client.Core
{
    public class MessageProcessor
    {
        private readonly MessageRepository _messageRepository;
        private readonly ChatRepository _chatRepository;
        private readonly UserRepository _userRepository;
        private readonly RequestService _requestService;
        private readonly IMessageSender _messageSender;
        private IDisposable _messageSubscription;

        public MessageProcessor(IMessageSender messageSender)
        {
            _messageRepository = new MessageRepository();
            _chatRepository = new ChatRepository();
            _userRepository = new UserRepository();
            _requestService = new RequestService();
            _messageSender = messageSender;
        }

        public void SubscribeForMessages(IObservable<BaseProtocolDto> messageObservable)
        {
            _messageSubscription = messageObservable.Subscribe(ProcessMessage);
        }

        private async void ProcessMessage(BaseProtocolDto message)
        {
            switch (message.MessageType)
            {
                case MessageTypes.PlainMassage:
                    var plainMessage = (PlainMessageDto)message;
                    switch (message.Chat.ChatTypes)
                    {
                        case ChatTypes.PtpChat:
                            await PtpChatIsExists(message, plainMessage);
                            break;
                        case ChatTypes.GroupChat:
                            await GroupChatIsExists(message, plainMessage);
                            break;
                    }
                    _messageRepository.SavePlainMessage(plainMessage);
                    var receivedPlainMessage = new WasReceiveMessageDto
                    {
                        MessageId = plainMessage.Id,
                        Sender = _userRepository.GetUser().Id
                    };
                    _messageSender.SendWasReceiveMessage(receivedPlainMessage);
                    break;
                case MessageTypes.WasReceive:
                    var wasReceiveMessage = (WasReceiveMessageDto)message;
                    await _messageRepository.MessageReceived(wasReceiveMessage.MessageId);
                    break;
                case MessageTypes.MessagesRead:
                    var messagesRead = (MessagesReadDto)message;
                    _messageRepository.MessageRead(messagesRead.Chat.Id);
                    break;
                case MessageTypes.DeleteMessage:
                    var deleteMessage = (DeleteMessageDto)message;
                    _messageRepository.DeleteMessage(deleteMessage.MessageId);
                    break;
                case MessageTypes.MediaMessage:
                    var mediaMessage = (MediaMessageDto)message;
                    switch (message.Chat.ChatTypes)
                    {
                        case ChatTypes.PtpChat:
                            await PtpChatIsExists(message, mediaMessage);
                            break;
                        case ChatTypes.GroupChat:
                            await GroupChatIsExists(message, mediaMessage);
                            break;
                    }
                    _messageRepository.SaveMediaMessage(mediaMessage);
                    var receivedMediaMessage = new WasReceiveMessageDto
                    {
                        MessageId = mediaMessage.Id,
                        Sender = _userRepository.GetUser().Id
                    };
                    _messageSender.SendWasReceiveMessage(receivedMediaMessage);                    
                    break;
            }
        }

        private async Task GroupChatIsExists(BaseProtocolDto message, BaseProtocolDto plainMessage)
        {
            if (!_chatRepository.GroupChatIsExists(message.Chat.Id))
            {
                var requestChatInformation = await _requestService.SendGetRequest
                    ($"http://{Constants.Ip}/chat/getgroupchatinformation?chatId={message.Chat.Id}");

                var chatInformation = JsonSerializer.Deserialize<GroupChatInformation>
                    (requestChatInformation, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new GroupChatInformation();

                var users = new List<User>();
                foreach (var user in chatInformation.Users)
                {
                    users.Add(new User
                    {
                        Id = user.Id,
                        Username = user.Name
                    });
                }
                await _chatRepository.SaveChat(new GroupChat
                {
                    Id = plainMessage.Chat.Id,
                    NameChat = chatInformation.NameChat,
                    Users = users
                });
            }
        }

        private async Task PtpChatIsExists(BaseProtocolDto message, BaseProtocolDto plainMessage)
        {
            if (!_chatRepository.PtpChatIsExists(message.Chat.Id))
            {
                var requestUser = await _requestService.SendGetRequest($"http://{Constants.Ip}/user/getuser?userId={message.Sender}");
                var user = JsonSerializer.Deserialize<UserModel>
                    (requestUser, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new UserModel();
                await _chatRepository.SaveChat(new PtpChat
                {
                    Id = plainMessage.Chat.Id,
                    FirstUserId = _userRepository.GetUser().Id,
                    SecondUser = new User
                    {
                        Id = user.Id,
                        Username = user.Username,
                        Login = user.Login
                    }
                });
            }
        }
    }
}
