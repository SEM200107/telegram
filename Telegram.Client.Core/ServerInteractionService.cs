﻿using Microsoft.Extensions.Logging;
using System.Net.WebSockets;
using System.Text.Json;
using Telegram.Client.Database;
using Telegram.Core.Services.Models;
using Telegram.Core.Services.Models.Messages;
using Telegram.Core.Services.Models.Requests;
using Telegram.Core.Services.Services;

namespace Telegram.Client.Core
{
    public class ServerInteractionService : IMessageSender
    {
        public IObservable<BaseProtocolDto> MessageObservable => _listener.MessageObservable;
        private readonly LoggerProvider _loggerProvider;
        private ClientMessagesListener _listener;
        private readonly SynchronizationReposinory _synchronizationReposinory;
        private readonly RequestService _requestService;

        public ServerInteractionService(LoggerProvider loggerProvider)
        {
            _loggerProvider = loggerProvider;
            _synchronizationReposinory = new SynchronizationReposinory();
            _requestService = new RequestService();
        }
        public async Task<bool> Connect(string url, CancellationToken cancellationToken, string token, Guid userId)
        {
            try
            {
                var socket = new ClientWebSocket();
                socket.Options.SetRequestHeader("Authorization", $"Bearer {token}");
                await socket.ConnectAsync(new Uri(url), cancellationToken);
                _listener = new ClientMessagesListener(socket, _loggerProvider.GetLogger<ClientMessagesListener>());
                var startListen = _listener.StartListen();
                var messageProcessor = new MessageProcessor(this);
                messageProcessor.SubscribeForMessages(MessageObservable);

                var synchronizationModel = _synchronizationReposinory.GetSynchronizationModel();
                var content = await _requestService.SendPostRequest(synchronizationModel, $"http://{Constants.Ip}/synchronization");
                var response = JsonSerializer.Deserialize<SynchronizationModelResponse>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }) ?? new SynchronizationModelResponse();
                await _synchronizationReposinory.Synchronization(response);
            }
            catch (Exception ex)
            {
                var logger = _loggerProvider.GetLogger<ServerInteractionService>();
                logger.LogError(ex.ToString());
                return false;
            }
            return true;
        }

        public void SendPlainMessage(PlainMessageDto model)
        {
            _listener.EnqueueMessage(model);
        }

        public void SendWasReceiveMessage(WasReceiveMessageDto model)
        {
            _listener.EnqueueMessage(model);
        }

        public void SendMessagesRead(MessagesReadDto model)
        {
            _listener.EnqueueMessage(model);
        }
        public void SendDeleteMessage(DeleteMessageDto model)
        {
            _listener.EnqueueMessage(model);
        }

        public void SendMediaMessage(MediaMessageDto model)
        {
            _listener.EnqueueMessage(model);
        }

        public void StopConnection()
        {
            _listener.Stop();
        }
    }
}