﻿namespace Telegram.Client.Core.Models
{
    public class UserModel
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Login { get; set; }
    }
}
