﻿namespace Telegram.Core.Services.Models.Enums
{
    public enum DeleteMessageTypes : short
    {
        JustMe = 1,
        EveryoneHas = 2 
    }
}
