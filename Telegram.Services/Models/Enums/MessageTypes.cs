﻿namespace Telegram.Core.Services.Models.Enums
{
    public enum MessageTypes: short
    {
        PlainMassage = 1,
        WasReceive = 2,
        MessagesRead = 3,
        DeleteMessage = 4,
        MediaMessage = 5        
    }
}
