﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telegram.Core.Services.Models.Enums
{
    public enum MediaTypes: short
    {
        PhotoMessage = 1,
        FileMessage = 2
    }
}
