﻿namespace Telegram.Core.Services.Models.Enums
{
    public enum ChatTypes : short
    {
        PtpChat = 1,
        GroupChat,
        ChannelChat
    }
}
