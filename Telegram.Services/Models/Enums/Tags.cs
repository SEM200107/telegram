﻿namespace Telegram.Core.Services.Models.Enums
{
    public enum Tags : short
    {
        StartTag = 1,
        SenderTag ,
        ContentTag ,
        MessageIdTag,
        ChatTag,
        PtpChatTag,
        GroupChatTag,
        ChatIdTag,
        UserIdTag,
        DeleteTag,
        FileIdTag,
        FileTag,
        ReplyIdTag
    }
}
