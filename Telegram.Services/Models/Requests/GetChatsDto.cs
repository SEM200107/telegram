﻿namespace Telegram.Core.Services.Models.Requests
{
    public class GetChatsDto
    {
        public List<Guid> ChatsList { get; set; }
        public Guid Sender { get; set; }
    }
}
