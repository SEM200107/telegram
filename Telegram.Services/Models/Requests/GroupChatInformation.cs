﻿namespace Telegram.Core.Services.Models.Requests
{
    public class GroupChatInformation
    {
        public Guid ChatId { get; set; }
        public string NameChat { get; set; }
        public List<UserInformation> Users { get; set; }
    }

    public class UserInformation
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
    }
}
