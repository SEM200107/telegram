﻿namespace Telegram.Core.Services.Models.Requests
{
    public class RegistrationDto
    {
        public string Username { get; set; }
        public string Login { get; set; }
        public byte[] Password { get; set; }        
    }
}
