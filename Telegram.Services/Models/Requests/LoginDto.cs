﻿namespace Telegram.Core.Services.Models.Requests
{
    public class LoginDto
    {
        public string Login { get; set; }
        public byte[] Password { get; set; }
    }
}
