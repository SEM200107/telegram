﻿namespace Telegram.Core.Services.Models.Requests
{
    public class SearchChatDto
    {
        public string SearchString { get; set; }
        public Guid UserId { get; set; }
    }
}
