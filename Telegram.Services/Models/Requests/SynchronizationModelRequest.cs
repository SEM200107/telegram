﻿namespace Telegram.Core.Services.Models.Requests
{
    public class SynchronizationModelRequest
    {
        public Guid UserId { get; set; }
        public List<Guid> PtpChatsId { get; set; } = new List<Guid>();
        public List<Guid> GroupChatsId { get; set; } = new List<Guid>();
        public List<Guid> MessagesId { get; set; } = new List<Guid>();
    }
}
