﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Services.Models.Requests
{
    public class SynchronizationModelResponse
    {
        public List<SynchronizationPtpChatModel> PtpChats { get; set; } = new List<SynchronizationPtpChatModel>();
        public List<GroupChatInformation> GroupChats { get; set; } = new List<GroupChatInformation>(); 
        public List<SynchronizationMessageModel> Messages { get; set; } = new List<SynchronizationMessageModel>(); 
    }

    public class SynchronizationPtpChatModel
    {
        public Guid UserId { get; set; }
        public Guid ChatId { get; set; }
        public string Username { get; set; }
        public string Login { get; set; }
    }

    public class SynchronizationMessageModel
    {
        public Guid MessageId { get; set; }
        public Guid ChatId { get; set; }
        public Guid SenderId { get; set; }
        public DateTime DateOfReceiving { get; set; }
        public string Content { get; set; }
        public Guid FileId { get; set; }
        public MediaTypes MediaType { get; set; }
        public Guid Reply { get; set; }
    }
}
