﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Services.Models.Requests
{
    public class FileRequest
    {
        public StreamContent File { get; set; }
        public MediaTypes MediaType { get; set; }
        public Guid FileId { get; set; }
    }
}
