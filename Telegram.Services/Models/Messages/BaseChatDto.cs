﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Services.Models.Messages
{
    public abstract class BaseChatDto
    {
        public Guid Id { get; set; }
        public abstract ChatTypes ChatTypes { get; }
    }
}
