﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Services.Models.Messages
{
    public class GroupChatDto : BaseChatDto
    {
        public override ChatTypes ChatTypes => ChatTypes.GroupChat;
        public List<Guid> UsersId { get; set; }
    }
}
