﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Services.Models.Messages
{
    public class MessagesReadDto : BaseProtocolDto
    {
        public override MessageTypes MessageType => MessageTypes.MessagesRead;
    }
}
