﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Services.Models.Messages
{
    public class WasReceiveMessageDto : BaseProtocolDto
    {
        public override MessageTypes MessageType => MessageTypes.WasReceive;
        public Guid MessageId { get; set; }
    }
}
