﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Services.Models.Messages
{
    public class MediaMessageDto : BaseProtocolDto
    {
        public string Content { get; set; }
        public Guid FileId { get; set; }
        public MediaTypes MediaType { get; set; }
        public Guid Reply { get; set; }
        public override MessageTypes MessageType => MessageTypes.MediaMessage;
    }
}
