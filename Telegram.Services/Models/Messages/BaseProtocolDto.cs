﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Services.Models.Messages
{
    public abstract class BaseProtocolDto
    {
        public Guid Id { get; set; }
        public abstract MessageTypes MessageType { get; }

        public BaseChatDto Chat { get; set; }
        public Guid Sender { get; set; }
        
    }
}
