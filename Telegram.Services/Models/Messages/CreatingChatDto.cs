﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Services.Models.Messages
{
    public class CreatingChatDto
    {
        public List<Guid> UsersId { get; set; }
        public ChatTypes ChatTypes { get; set; }
        public string NameChat { get; set; }
    }
}
