﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Services.Models.Messages
{
    public class PtpChatDto : BaseChatDto
    {
        public override ChatTypes ChatTypes => ChatTypes.PtpChat;
    }
}
