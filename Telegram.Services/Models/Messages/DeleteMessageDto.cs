﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Services.Models.Messages
{
    public class DeleteMessageDto : BaseProtocolDto
    {
        public Guid MessageId { get; set; }
        public DeleteMessageTypes DeleteMessageType { get; set; }
        public override MessageTypes MessageType => MessageTypes.DeleteMessage;
    }
}
