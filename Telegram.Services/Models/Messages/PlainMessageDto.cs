﻿using Telegram.Core.Services.Models.Enums;

namespace Telegram.Core.Services.Models.Messages
{
    public class PlainMessageDto : BaseProtocolDto
    {
        public override MessageTypes MessageType => MessageTypes.PlainMassage;
        public string Content { get; set; }
        public Guid Reply { get; set; }
    }
}
