﻿namespace Telegram.Core.Services.Models
{
    public static class Constants
    {
        public static Guid ServerGuid = Guid.Parse("ffffffff-ffff-ffff-ffff-ffffffffffff");
        public static string Ip = "localhost:5186";
    }
}
