﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Telegram.Core.Services.Services
{
    public class LoggerProvider
    {
        private readonly IServiceProvider _serviceProvider;

        public LoggerProvider(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public ILogger<T> GetLogger<T>()
        {
            return _serviceProvider.GetService<ILogger<T>>();
        }
    }
}
