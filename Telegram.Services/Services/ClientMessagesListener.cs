﻿using System.Collections.Concurrent;
using System.Net.WebSockets;
using System.Reactive.Subjects;
using Microsoft.Extensions.Logging;
using Telegram.Core.Services.Models.Messages;

namespace Telegram.Core.Services.Services
{
    public class ClientMessagesListener
    {
        public IObservable<BaseProtocolDto> MessageObservable => _messageSubject;

        private readonly Queue<BaseProtocolDto> _messages;
        private readonly WebSocket _socket;
        private readonly ILogger _logger;
        private readonly MessageEncoderDecoder _messageEncoderDecoder;
        public byte[] _buffer;
        private readonly Subject<BaseProtocolDto> _messageSubject;
        private CancellationTokenSource _cancellationTokenSourse;
        private readonly ConcurrentQueue<byte> _bytesToDecode;

        public ClientMessagesListener(WebSocket socket, ILogger<ClientMessagesListener> logger)
        {
            _messages = new Queue<BaseProtocolDto>();
            _socket = socket;
            _logger = logger;
            _messageEncoderDecoder = new MessageEncoderDecoder();
            _buffer = new byte[1024 * 100];
            _messageSubject = new Subject<BaseProtocolDto>();
            _bytesToDecode = new ConcurrentQueue<byte>();
        }
        public async Task StartListen()
        {
            _cancellationTokenSourse = new CancellationTokenSource();
            var cancellationToken = _cancellationTokenSourse.Token;

            var sendingTask = StartSending(cancellationToken);
            var receivingTask = StartReceiving(cancellationToken);
            var decodeTask = StartDecode(cancellationToken);

            await Task.WhenAll(sendingTask, receivingTask, decodeTask);
        }

        public void Stop()
        {
            _cancellationTokenSourse.Cancel();
            _socket.Dispose();
            _messageSubject.OnCompleted();
        }

        public void EnqueueMessage(BaseProtocolDto message)
        {
            _messages.Enqueue(message);
        }

        private async Task StartSending(CancellationToken cancellationToken)
        {
            try
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    while (_messages.TryDequeue(out var messageToSend))
                    {
                        var bytesMessage = _messageEncoderDecoder.Encode(messageToSend);                        
                        await _socket.SendAsync(bytesMessage, WebSocketMessageType.Binary, false, cancellationToken);
                    }
                    await Task.Delay(100, cancellationToken);
                }
            }
            catch (TaskCanceledException)
            {
                return;
            }
            catch (WebSocketException)
            {
                await _socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Close", cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
            }
        }

        private async Task StartReceiving(CancellationToken cancellationToken)
        {
            try
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    if (_bytesToDecode.Count >= _buffer.Length)
                    {
                        await Task.Delay(100, cancellationToken);
                    }
                    var receiveResult = await _socket.ReceiveAsync(_buffer, cancellationToken);
                    var receiveCount = receiveResult.Count;

                    if (receiveCount != 0)
                    {
                        for (var i = 0; i < receiveCount; i++)
                        {
                            _bytesToDecode.Enqueue(_buffer[i]);
                        }
                        Array.Clear(_buffer);
                    }
                }
            }
            catch (TaskCanceledException)
            {
                return;
            }
            catch (WebSocketException)
            {
                await _socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Close", cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
            }            
        }

        private async Task StartDecode(CancellationToken cancellationToken)
        {
            try
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    if (_bytesToDecode.IsEmpty)
                    {
                        await Task.Delay(100, cancellationToken);
                        continue;
                    }
                    var message = _messageEncoderDecoder.Decode(_bytesToDecode);                    
                    _messageSubject.OnNext(message);
                }
            }
            catch (TaskCanceledException)
            {
                return;
            }
            catch (WebSocketException)
            {
                await _socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Close", cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
            }
        }       
    }
}
