﻿using System.Collections.Concurrent;
using System.Text;
using Telegram.Core.Services.Models.Enums;
using Telegram.Core.Services.Models.Messages;

namespace Telegram.Core.Services.Services
{
    public class MessageEncoderDecoder
    {
        public byte[] Encode(BaseProtocolDto model)
        {
            var result = new List<byte>();
            switch (model.MessageType)
            {
                case MessageTypes.PlainMassage:
                    EncodePlainMassage(result, (PlainMessageDto)model);
                    break;
                case MessageTypes.WasReceive:
                    EncodeWasSendMessage(result, (WasReceiveMessageDto)model);
                    break;
                case MessageTypes.MessagesRead:
                    EncodeMessagesRead(result, (MessagesReadDto)model);
                    break;
                case MessageTypes.DeleteMessage:
                    EncodeDeleteMessage(result, (DeleteMessageDto)model);
                    break;
                case MessageTypes.MediaMessage:
                    EncodeMediaMessage(result, (MediaMessageDto)model);
                    break;
            }

            return result.ToArray();
        }

        public BaseProtocolDto Decode(ConcurrentQueue<byte> bytes)
        {
            var startTag = DequeueTag(bytes);

            if ((Tags)startTag != Tags.StartTag)
            {
                return null;
            }
            var length = LengthMessage(bytes);

            var inforationTag = DequeueTag(bytes);
            switch ((MessageTypes)inforationTag)
            {
                case MessageTypes.PlainMassage:
                    return DecodePlainMessage(bytes);
                case MessageTypes.WasReceive:
                    return DecodeWasSendMessage(bytes);
                case MessageTypes.MessagesRead:
                    return DecodeMessagesRead(bytes);
                case MessageTypes.DeleteMessage:
                    return DecodeDeleteMessage(bytes);
                case MessageTypes.MediaMessage:
                    return DecodeMediaMessage(bytes);

                default: return null;
            }
        }

        private void EncodePlainMassage(List<byte> result, PlainMessageDto model)
        {
            var bytesSender = model.Sender.ToByteArray();
            var bytesChat = EncodeChat(model.Chat);
            var bytesIdMessage = model.Id.ToByteArray();
            var bytesContent = Encoding.UTF8.GetBytes(model.Content);
            var bytesReplyId = model.Reply.ToByteArray();

            InsertTag(result, (short)MessageTypes.PlainMassage);
            FormationTag(result, bytesSender, Tags.SenderTag);
            FormationTag(result, bytesChat, Tags.ChatTag);
            FormationTag(result, bytesContent, Tags.ContentTag);
            FormationTag(result, bytesIdMessage, Tags.MessageIdTag);
            FormationTag(result, bytesReplyId, Tags.ReplyIdTag);
            InsertStartTag(result);
        }

        private PlainMessageDto DecodePlainMessage(ConcurrentQueue<byte> bytes)
        {
            var sender = GetGuid(bytes, Tags.SenderTag);
            var chat = GetChat(bytes);
            var content = GetString(bytes, Tags.ContentTag);
            var id = GetGuid(bytes, Tags.MessageIdTag);
            var replyId = GetGuid(bytes, Tags.ReplyIdTag);

            var message = new PlainMessageDto
            {
                Id = id,
                Sender = sender,
                Chat = chat,
                Content = content,
                Reply = replyId
            };
            return message;
        }

        private void EncodeWasSendMessage(List<byte> result, WasReceiveMessageDto model)
        {
            var bytesSender = model.Sender.ToByteArray();
            var bytesMessageId = model.MessageId.ToByteArray();

            InsertTag(result, (short)MessageTypes.WasReceive);
            FormationTag(result, bytesSender, Tags.SenderTag);
            FormationTag(result, bytesMessageId, Tags.MessageIdTag);
            InsertStartTag(result);
        }

        private WasReceiveMessageDto DecodeWasSendMessage(ConcurrentQueue<byte> bytes)
        {
            var sender = GetGuid(bytes, Tags.SenderTag);
            var messageId = GetGuid(bytes, Tags.MessageIdTag);

            var wasSend = new WasReceiveMessageDto
            {
                MessageId = messageId,
                Sender = sender
            };
            return wasSend;
        }

        private void EncodeDeleteMessage(List<byte> result, DeleteMessageDto model)
        {
            var bytesSender = model.Sender.ToByteArray();
            var bytesMessageId = model.MessageId.ToByteArray();
            var bytesChat = EncodeChat(model.Chat);
            var bytesDeleteTag = new byte[2];
            bytesDeleteTag[0] = (byte)((short)model.DeleteMessageType >> 8);
            bytesDeleteTag[1] = (byte)((short)model.DeleteMessageType);

            InsertTag(result, (short)MessageTypes.DeleteMessage);
            FormationTag(result, bytesSender, Tags.SenderTag);
            FormationTag(result, bytesMessageId, Tags.MessageIdTag);
            FormationTag(result, bytesChat, Tags.ChatTag);
            FormationTag(result, bytesDeleteTag, Tags.DeleteTag);
            InsertStartTag(result);
        }

        private DeleteMessageDto DecodeDeleteMessage(ConcurrentQueue<byte> bytes)
        {
            var sender = GetGuid(bytes, Tags.SenderTag);
            var messageId = GetGuid(bytes, Tags.MessageIdTag);
            var chat = GetChat(bytes);
            var deleteTag = GetDeleteTag(bytes, Tags.DeleteTag);

            var deleteMessage = new DeleteMessageDto
            {
                MessageId = messageId,
                Sender = sender,
                Chat = chat,
                DeleteMessageType = (DeleteMessageTypes)deleteTag
            };
            return deleteMessage;
        }

        private void EncodeMessagesRead(List<byte> result, MessagesReadDto model)
        {
            var bytesSender = model.Sender.ToByteArray();
            var bytesChat = EncodeChat(model.Chat);

            InsertTag(result, (short)MessageTypes.MessagesRead);
            FormationTag(result, bytesSender, Tags.SenderTag);
            FormationTag(result, bytesChat, Tags.ChatTag);
            InsertStartTag(result);
        }

        private MessagesReadDto DecodeMessagesRead(ConcurrentQueue<byte> bytes)
        {
            var sender = GetGuid(bytes, Tags.SenderTag);
            var chat = GetChat(bytes);

            var result = new MessagesReadDto
            {
                Sender = sender,
                Chat = chat
            };
            return result;
        }

        private void EncodeMediaMessage(List<byte> result, MediaMessageDto model)
        {
            var bytesSender = model.Sender.ToByteArray();
            var bytesChat = EncodeChat(model.Chat);
            var bytesIdMessage = model.Id.ToByteArray();
            var bytesReplyId = model.Reply.ToByteArray();
            if (model.Content is null)
            {
                model.Content = "";
            }
            var bytesContent = Encoding.UTF8.GetBytes(model.Content);
            var bytesIdFile = model.FileId.ToByteArray();

            InsertTag(result, (short)MessageTypes.MediaMessage);
            FormationTag(result, bytesSender, Tags.SenderTag);
            FormationTag(result, bytesChat, Tags.ChatTag);
            FormationTag(result, bytesContent, Tags.ContentTag);
            FormationTag(result, bytesIdMessage, Tags.MessageIdTag);
            FormationTag(result, bytesReplyId, Tags.ReplyIdTag);
            FormationTag(result, bytesIdFile, Tags.FileIdTag);
            InsertTag(result, (short)model.MediaType);            
            InsertStartTag(result);
        }

        private MediaMessageDto DecodeMediaMessage(ConcurrentQueue<byte> bytes)
        {
            var sender = GetGuid(bytes, Tags.SenderTag);
            var chat = GetChat(bytes);
            var content = GetString(bytes, Tags.ContentTag);
            var id = GetGuid(bytes, Tags.MessageIdTag);
            var replyId = GetGuid(bytes, Tags.ReplyIdTag);
            var fileId = GetGuid(bytes, Tags.FileIdTag);
            var mediaType = (MediaTypes)DequeueTag(bytes);

            var mediaMessage = new MediaMessageDto
            {
                Sender = sender,
                Id = id,
                Content = content,
                MediaType = mediaType,
                Chat = chat,
                FileId = fileId,
                Reply = replyId
            };

            return mediaMessage;
        }

        private byte[] EncodeChat(BaseChatDto model)
        {
            var result = new List<byte>();
            var chatId = model.Id.ToByteArray();
            switch (model.ChatTypes)
            {
                case ChatTypes.PtpChat:
                    InsertTag(result, (short)Tags.PtpChatTag);
                    FormationTag(result, chatId, Tags.ChatIdTag);
                    InsertStartTag(result);
                    return result.ToArray();
                case ChatTypes.GroupChat:
                    var chat = (GroupChatDto)model;
                    InsertTag(result, (short)Tags.GroupChatTag);
                    FormationTag(result, chatId, Tags.ChatIdTag);
                    result.Add(Convert.ToByte(chat.UsersId.Count));
                    foreach (var userId in chat.UsersId)
                    {
                        FormationTag(result, userId.ToByteArray(), Tags.UserIdTag);
                    }
                    InsertStartTag(result);
                    return result.ToArray();
            }
            return null;
        }

        private BaseChatDto GetChat(ConcurrentQueue<byte> bytes)
        {
            var tag = DequeueTag(bytes);
            if ((Tags)tag != Tags.ChatTag)
            {
                return null;
            }
            bytes.TryDequeue(out var lengthChat);
            var startTag = DequeueTag(bytes);
            if ((Tags)startTag != Tags.StartTag)
            {
                return null;
            }
            var length = LengthMessage(bytes);
            var chatTag = DequeueTag(bytes);
            var chatId = GetGuid(bytes, Tags.ChatIdTag);
            switch ((Tags)chatTag)
            {
                case Tags.PtpChatTag:
                    var ptpChat = new PtpChatDto
                    {
                        Id = chatId
                    };
                    return ptpChat;
                case Tags.GroupChatTag:
                    var guidList = new List<Guid>();
                    bytes.TryDequeue(out var countUsers);
                    for (var i = 1; i <= countUsers; i++)
                    {
                        var id = GetGuid(bytes, Tags.UserIdTag);
                        guidList.Add(id);
                    }
                    var groupChat = new GroupChatDto
                    {
                        Id = chatId,
                        UsersId = guidList
                    };
                    return groupChat;
            }
            return null;
        }

        private void InsertStartTag(List<byte> result)
        {
            var startTag = (short)Tags.StartTag;
            var startTagBytes1 = (byte)(startTag >> 8);
            var startTagBytes2 = (byte)startTag;
            result.Insert(0, startTagBytes1);
            result.Insert(1, startTagBytes2);
            var length = result.Count;
            result.Insert(2, (byte)(length >> 24));
            result.Insert(3, (byte)(length >> 16));
            result.Insert(4, (byte)(length >> 8));
            result.Insert(5, (byte)length);
        }

        private int LengthMessage(ConcurrentQueue<byte> bytes)
        {
            bytes.TryDequeue(out var l1);
            bytes.TryDequeue(out var l2);
            bytes.TryDequeue(out var l3);
            bytes.TryDequeue(out var l4);
            return (l1 << 24) + (l2 << 16) + (l3 << 8) + l4;
        }

        private void FormationTag(List<byte> result, byte[] content, Tags tag)
        {
            InsertTag(result, (short)tag);
            result.Add(Convert.ToByte(content.Length));
            result.AddRange(content);
        }

        private void InsertByteFile(List<byte> result, byte[] content, Tags tag)
        {
            InsertTag(result, (short)tag);
            var length = content.Length;
            var bytesLength = new byte[4];
            bytesLength[0] = (byte)(length >> 24);
            bytesLength[1] = (byte)(length >> 16);
            bytesLength[2] = (byte)(length >> 8);
            bytesLength[3] = (byte)length;
            result.AddRange(bytesLength);
            result.AddRange(content);
        }

        private byte[] GetBytesFile(ConcurrentQueue<byte> bytes)
        {
            var Tag = DequeueTag(bytes);
            if ((Tags)Tag != Tags.FileTag)
            {
                return null;
            }
            bytes.TryDequeue(out var l1);
            bytes.TryDequeue(out var l2);
            bytes.TryDequeue(out var l3);
            bytes.TryDequeue(out var l4);
            var length = (l1 << 24) + (l2 << 16) + (l3 << 8) + l4;
            return TryDequeueRange(bytes, length);
        }

        private void InsertTag(List<byte> bytes, short tag)
        {
            var tagBytes1 = (byte)(tag >> 8);
            var tagBytes2 = (byte)tag;
            bytes.Add(tagBytes1);
            bytes.Add(tagBytes2);
        }

        private byte[] TryDequeueRange(ConcurrentQueue<byte> bytes, int capasity)
        {
            var listBytes = new List<byte>(capasity);
            for (var i = 0; i < capasity; i++)
            {
                bytes.TryDequeue(out var result);
                listBytes.Add(result);
            }
            return listBytes.ToArray();
        }

        private string GetString(ConcurrentQueue<byte> bytes, Tags tag)
        {
            var Tag = DequeueTag(bytes);
            if ((Tags)Tag != tag)
            {
                return null;
            }
            bytes.TryDequeue(out var length);
            var arrayBytes = TryDequeueRange(bytes, length);
            return Encoding.UTF8.GetString(arrayBytes);
        }

        private Guid GetGuid(ConcurrentQueue<byte> bytes, Tags tag)
        {
            var Tag = DequeueTag(bytes);
            if ((Tags)Tag != tag)
            {
                return new Guid();
            }
            bytes.TryDequeue(out var length);
            var arrayBytes = TryDequeueRange(bytes, length);
            return new Guid(arrayBytes);
        }

        private int GetDeleteTag(ConcurrentQueue<byte> bytes, Tags tag)
        {
            var Tag = DequeueTag(bytes);
            if ((Tags)Tag != tag)
            {
                return 0;
            }
            bytes.TryDequeue(out var length);
            var arrayBytes = TryDequeueRange(bytes, length);
            return (arrayBytes[0] << 8) + arrayBytes[1];
        }

        private int DequeueTag(ConcurrentQueue<byte> bytes)
        {
            bytes.TryDequeue(out var tag1);
            bytes.TryDequeue(out var tag2);
            return (tag1 << 8) + tag2;
        }
    }
}
